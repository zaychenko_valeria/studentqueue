package studentqueue.utils;

public class NonEmptyString {


    public NonEmptyString( String value )
    {
        setValue( value );
    }

     public void setValue( String value )
     {
         if ( value == null )
             throw new RuntimeException( "String is null" );
         if ( value.isEmpty() )
             throw new RuntimeException( "String is empty" );
         this.value = value;
     }


     public String getValue()
     {
         return this.value;
     }

     private String value;
}
