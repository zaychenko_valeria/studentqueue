package studentqueue.utils;

/**
 * Created by lera on 23.03.2016.
 */
public final class EnumUtils
{
    public static < T extends Enum< T > > T getEnumFromString( Class< T > c, String string) {
        if( c != null && string != null ) {
            try
            {
                return Enum.valueOf( c, string.trim().toUpperCase() );
            }
            catch( IllegalArgumentException ex )
            {
                throw new RuntimeException( "Illegal" );
            }
        }
        return null;
    }
}
