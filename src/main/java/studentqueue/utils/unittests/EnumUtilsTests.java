package studentqueue.utils.unittests;

import static org.junit.Assert.*;

import org.junit.Test;
import studentqueue.domain.StudyForm;
import studentqueue.domain.StudyFormType;
import studentqueue.utils.EnumUtils;

/**
 * Created by lera on 23.03.2016.
 */
public class EnumUtilsTests
{
    @Test
    public void createFromStringEnumCorrectly()
    {
        StudyFormType type = EnumUtils.getEnumFromString( StudyFormType.class , "LECTURE_TYPE" );
        assertEquals( StudyFormType.LECTURE_TYPE , type );
    }

    @Test( expected = Exception.class )
    public void dontCreateFromEmptyString()
    {
        StudyFormType type = EnumUtils.getEnumFromString( StudyFormType.class , "" );
    }

    @Test( expected = Exception.class )
    public void dontCreateFromIncorrectName()
    {
        StudyFormType type = EnumUtils.getEnumFromString( StudyFormType.class , "lab" );
    }

    @Test
    public void createFromLowerCaseStringCorrect()
    {
        StudyFormType type = EnumUtils.getEnumFromString( StudyFormType.class , "lab_type" );
        assertEquals( StudyFormType.LAB_TYPE , type );
    }

}
