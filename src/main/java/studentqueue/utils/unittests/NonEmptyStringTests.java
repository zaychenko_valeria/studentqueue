package studentqueue.utils.unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import studentqueue.utils.NonEmptyString;

public class NonEmptyStringTests {

    @Test
    public void constructorInitsCorrectly()
    {
        String test = "This is a test string";
        NonEmptyString testString = new NonEmptyString( test );

        assertEquals( test , testString.getValue() );
    }


    @Test( expected = Exception.class )
    public void exceptionWhenSendNull()
    {
        String test = null;
        new NonEmptyString( test );
    }


    @Test( expected = Exception.class )
    public void exceptionWhenSendEmpty()
    {
        String test = "";
        new NonEmptyString( test );
    }
}
