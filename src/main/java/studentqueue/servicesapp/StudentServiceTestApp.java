package studentqueue.servicesapp;

/**
 * Created by lera on 15.02.2016.
 */
public final class StudentServiceTestApp
{
    public static void createStudentControllerTestApp()
    {
        createStudentPetrov();
        changePetrovEmail();

        createStudentIvanov();
        changeIvanovName();
    }


    public static String getUuidPetrov ()
    {
        return uuidPetrov;
    }

    public static String getUuidIvanov ()
    {
        return uuidIvanov;
    }


    public static void createStudentPetrov()
    {
        uuidPetrov = TestApp.istudentController.createStudent( "Vasya" , "Petrov" , "pet@ya.ru" );
    }

    public static void changePetrovEmail()
    {
        TestApp.istudentController.changeAccountEmail( uuidPetrov , "hhh@YA.RU" );
    }

    public static void createStudentIvanov()
    {
        uuidIvanov = TestApp.istudentController.createStudent( "Vanya" , "Ivanov" , "iao@ru" );
    }

    public static void changeIvanovName()
    {
        TestApp.istudentController.changeAccountFirstName( uuidIvanov , "Ihor" );
    }



    static String uuidPetrov;
    static String uuidIvanov;
}
