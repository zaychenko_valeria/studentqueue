package studentqueue.servicesapp;

/**
 * Created by lera on 18.02.2016.
 */
public class InstructorServiceTestApp
{
    public static void createInstructorServiceTestApp()
    {
        createLecturer1();
        createAssistant1();
        createAssistant2();
    }

    public static void createLecturer1()
    {
        uuidLecturer1 = TestApp.iInstructorService.createInstructor( "AN" , "KIRILLOV" , "anna" );
    }

    public static void createAssistant1()
    {
        uuidAssistant1 = TestApp.iInstructorService.createInstructor( "BM" , "IHOROV" , "ihb" );
    }

    public static void createAssistant2()
    {
        uuidAssistant2 = TestApp.iInstructorService.createInstructor( "aa" , "aaaaaa" , "aaa" );
    }


    static String uuidLecturer1;
    static String uuidAssistant1;
    static String uuidAssistant2;
}
