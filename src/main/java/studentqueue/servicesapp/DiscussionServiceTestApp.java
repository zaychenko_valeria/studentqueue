package studentqueue.servicesapp;

import studentqueue.viewmodel.DiscussionMessageView;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by lera on 09.03.2016.
 */
//@Service
public class DiscussionServiceTestApp
{
    public static void createDiscussuionControllerTestApp() throws IOException
    {
        createDiscussion();
        writeMessage1();
        writeMessage2();

        DiscussionMessageView discussionMessageView = TestApp.iDiscussionService.viewMessageHistory( uuidDiscussion );
        printMessages( discussionMessageView );
    }


    public static void printMessages( DiscussionMessageView discussionMessageView )
    {
        System.out.print( discussionMessageView.getSubject() );
        System.out.print( "-" );
        System.out.print( discussionMessageView.getRecipient() );
        Iterator< DiscussionMessageView.MessageView > messageViewIterator = discussionMessageView.getMessageViewsIterator();
        while ( messageViewIterator.hasNext() )
        {
            DiscussionMessageView.MessageView messageView = messageViewIterator.next();
            System.out.print( messageView.getSender() );
            System.out.print( "-" );
            System.out.print( messageView.getText() );
            System.out.print( "-" );
            System.out.print( messageView.getArrivedAt() );
        }
        System.out.print( " " );
    }



    public static void createDiscussion()
    {
        uuidDiscussion = TestApp.iDiscussionService.createDiscussion( "IPZ question" ,
                                                        StudentServiceTestApp.getUuidIvanov() ,
                                                        InstructorServiceTestApp.uuidAssistant1 ,
                                                        "I have a problem with the code, please look it" );
    }


    public static void writeMessage1()
    {
        TestApp.iDiscussionService.writeMessage( uuidDiscussion , "Problem 1 in file .bat" , StudentServiceTestApp.getUuidIvanov() );
    }


    public static void writeMessage2()
    {
        TestApp.iDiscussionService.writeMessage( uuidDiscussion , "Problem 2" , StudentServiceTestApp.getUuidIvanov() );
    }


    public static String uuidDiscussion;
}
