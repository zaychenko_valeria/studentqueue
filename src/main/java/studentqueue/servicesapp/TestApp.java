package studentqueue.servicesapp;

import java.io.IOException;

import studentqueue.domain.Account;
import studentqueue.repository.*;
import studentqueue.repository.impl.*;
import studentqueue.services.*;
import studentqueue.services.impl.*;


public class TestApp {

    public static void main(String[] args) throws IOException{

        DisciplineServiceTestApp.createDisciplineServiceTestApp();
        StudentServiceTestApp.createStudentControllerTestApp();
        InstructorServiceTestApp.createInstructorServiceTestApp();
        GroupServiceTestApp.createGroupControllerTestApp();
        OfferingServiceTestApp.createOfferingControllerTestApp();
        DiscussionServiceTestApp.createDiscussuionControllerTestApp();

        //Database.close();
    }


//*******************************************************************************************


    static IStudentRepository studentRepository = new StudentRepository();
    static IGroupRepository groupRepository = new GroupRepository();
    static IDisciplineRepository disciplineRepository = new DisciplineRepository();
    static IOfferingRepository offeringRepository = new OfferingRepository();
    static IInstructorRepository instructorRepository = new InstructorRepository();
    static IDiscussionRepository discussionRepository = new DiscussionRepository();
    static IAccountRepository accountRepository = new AccountRepository< Account >( Account.class , "Account" );

    static IStudentService istudentController = new StudentService();
    static IGroupService iGroupController = new GroupService();
    static IDisciplineService idisciplineController = new DisciplineService();
    static IDisciplineOfferingService iDisciplineOfferingService = new DisciplineOfferingService();
    static IInstructorService iInstructorService = new InstructorService();
    static IDiscussionService iDiscussionService = new DiscussionService();
}