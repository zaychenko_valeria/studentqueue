package studentqueue.servicesapp;

/**
 * Created by lera on 15.02.2016.
 */
public final class OfferingServiceTestApp
{
    public static void createOfferingControllerTestApp()
    {
        createOffering20162017();
        assignKI10toOffering();
        setExamFormControl();
        assignKI11toOffering();
        changeOfferingDates();
        addFirstAssistant();
        addSecondAssistant();
        changeAssistantToLecturer();
    }


    private static void createOffering20162017()
    {
        uuidOffering = TestApp.iDisciplineOfferingService.createOffering(
                            "26-02-2016" , "26-02-2017" ,
                            "IPZ" , "WRITTEN_EXAMINATION" , InstructorServiceTestApp.uuidLecturer1 );
    }

    private static void assignKI10toOffering()
    {
        TestApp.iDisciplineOfferingService.assignGroupToOffering( "KI-10-5" , uuidOffering );
    }

    private static void setExamFormControl()
    {
        TestApp.iDisciplineOfferingService.setFormOfControl( "KI-10-5" , "CREDIT" , uuidOffering );
    }

    private static void assignKI11toOffering()
    {
        TestApp.iDisciplineOfferingService.assignGroupToOffering( "KI-11" , uuidOffering );
    }

    private static void changeOfferingDates()
    {
        TestApp.iDisciplineOfferingService.changeDates( "22-03-2018" , "23-07-2019" , uuidOffering );
    }

    private static void addFirstAssistant()
    {
        TestApp.iDisciplineOfferingService.addAssistant( uuidOffering , InstructorServiceTestApp.uuidAssistant1 );
    }

    private static void addSecondAssistant()
    {
        TestApp.iDisciplineOfferingService.addAssistant( uuidOffering , InstructorServiceTestApp.uuidAssistant2 );
    }

    private static void changeAssistantToLecturer()
    {
        TestApp.iDisciplineOfferingService.changeLecturer( uuidOffering , InstructorServiceTestApp.uuidAssistant2 );
    }

    public static String uuidOffering;
}
