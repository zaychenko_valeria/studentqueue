package studentqueue.servicesapp;

import studentqueue.viewmodel.EditDisciplineView;
import studentqueue.viewmodel.ScoringRulesView;
import studentqueue.viewmodel.StudyFormView;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by lera on 15.02.2016.
 */
public final class DisciplineServiceTestApp
{
    public static void createDisciplineServiceTestApp() throws IOException
    {
        createDisciplineIPZ();
        createLectureStudyFormIPZ1();
        createLabStudyFormIPZ1();
        createLabPointIPZ1();
        createTestPointIPZ1();
        addLevelBeginnerToLabIPZ();
        addLevelAdvancedToLabIPZ();
        addLevelIntermediateToTestIPZ();

        createDisciplineOOP();
        createLectureStudyFormOOP1();
        createLabStudyFormOOP1();
        createLabPointOOP1();
        createTestPointOOP1();
        addLevelIntermediateToLabOOP();
        addLevelAdvancedToLabOOP();
        addLevelBeginnerToTestOOP();


        EditDisciplineView disciplineViewIPZ = TestApp.idisciplineController.queryDiscipline( "IPZ" );
        EditDisciplineView disciplineViewOOP = TestApp.idisciplineController.queryDiscipline( "OOP" );
        printScoringRuleView( disciplineViewIPZ );
        printScoringRuleView( disciplineViewOOP );
        printStudyFormView( disciplineViewIPZ );
        printStudyFormView( disciplineViewOOP );


        //PrintWriter out = new PrintWriter( "DisciplineReport.out" );
        //out.println( "hello" );
        //out.close();
    }

    public static void printScoringRuleView( EditDisciplineView disciplineView )
    {
        Iterator<ScoringRulesView.AssignmentScoringRules> iter = disciplineView.getScoringRulesView().getCheckPointRuleIterator();
        while ( iter.hasNext() )
        {
            ScoringRulesView.AssignmentScoringRules rule = iter.next();
            int k = rule.getColumnsCount();
            for( int i = 0 ; i < k ; i++ )
            {
                System.out.print( rule.getLowRange( i ) );
                System.out.print( "-" );
                System.out.print( rule.getHighRange( i ) );
                System.out.print( " " );
            }
            System.out.println();
        }
    }


    public static void printStudyFormView( EditDisciplineView disciplineView )
    {
        Iterator< StudyFormView.StudyFormViewRule > iter = disciplineView.getStudyFormView().getStudyFormViewIterator();
        while ( iter.hasNext() )
        {
            StudyFormView.StudyFormViewRule rule = iter.next();
            System.out.print( rule.getStudyFormName() );
            System.out.print( " --- " );
            System.out.print( rule.getStudyFormQuantity() );
            System.out.print( " , " );
        }
        System.out.println();
    }


    public static void createDisciplineIPZ()
    {
        TestApp.idisciplineController.createDiscipline( "IPZ" );
    }


    public static void createDisciplineOOP()
    {
        TestApp.idisciplineController.createDiscipline( "OOP" );
    }


    public static void createLectureStudyFormIPZ1()
    {
        TestApp.idisciplineController.createStudyForm( "Lecture 1 IPZ" , "IPZ" , "LECTURE_TYPE" );
    }


    public static void createLectureStudyFormOOP1()
    {
        TestApp.idisciplineController.createStudyForm( "Lecture 1 OOP" , "OOP" , "LECTURE_TYPE" );
    }


    public static void createLabStudyFormIPZ1()
    {
        TestApp.idisciplineController.createStudyForm( "Lab 1 IPZ" , "IPZ" , "LAB_TYPE" );
    }


    public static void createLabStudyFormOOP1()
    {
        TestApp.idisciplineController.createStudyForm( "Lab 1 OOP" , "OOP" , "LAB_TYPE" );
    }


    public static void createLabPointIPZ1()
    {
        TestApp.idisciplineController.createLabAssignment( "Lab 1 IPZ" , "IPZ" );
    }


    public static void createTestPointIPZ1()
    {
        TestApp.idisciplineController.createTestAssignment( "IPZ" , "Test 1 IPZ" );
    }


    public static void createLabPointOOP1()
    {
        TestApp.idisciplineController.createLabAssignment( "Lab 1 OOP" , "OOP" );
    }


    public static void createTestPointOOP1()
    {
        TestApp.idisciplineController.createTestAssignment( "OOP" , "Test 1 OOP" );
    }


    public static void addLevelBeginnerToLabIPZ()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "IPZ" , "BEGINNER" , 10 , 30 , "Lab 1 IPZ" );
    }


    public static void addLevelAdvancedToLabIPZ()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "IPZ" , "ADVANCED" , 40 , 60 , "Lab 1 IPZ" );
    }


    public static void addLevelIntermediateToTestIPZ()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "IPZ" , "INTERMEDIATE" , 75 , 89 , "Test 1 IPZ" );
    }

    public static void addLevelIntermediateToLabOOP()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "OOP" , "INTERMEDIATE" , 20 , 50 , "Lab 1 OOP" );
    }


    public static void addLevelAdvancedToLabOOP()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "OOP" , "ADVANCED" , 40 , 50 , "Lab 1 OOP" );
    }


    public static void addLevelBeginnerToTestOOP()
    {
        TestApp.idisciplineController.addLeveltoAssignment( "OOP" , "BEGINNER" , 60 , 74 , "Test 1 OOP" );
    }
}
