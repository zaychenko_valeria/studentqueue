package studentqueue.servicesapp;

/**
 * Created by lera on 15.02.2016.
 */
public final class GroupServiceTestApp
{
    public static void createGroupControllerTestApp()
    {
        createGroupKI10();
        addStudentPetrovToGroupKi10();
        assignPetrovToCaptain();
        changeGroupNameKi10();
        addStudentIvanovToGroupKi10();

        createGroupKI11();
    }


    public static void createGroupKI10()
    {
        TestApp.iGroupController.createGroup( "KI-10" );
    }

    public static void addStudentPetrovToGroupKi10()
    {
        TestApp.iGroupController.addStudentToGroup( StudentServiceTestApp.getUuidPetrov() , "KI-10" );
    }

    public static void assignPetrovToCaptain()
    {
        TestApp.iGroupController.assignCaptain( StudentServiceTestApp.getUuidPetrov() , "KI-10" );
    }

    public static void changeGroupNameKi10()
    {
        TestApp.iGroupController.changeGroupName( "KI-10" , "KI-10-5" );
    }


    public static void addStudentIvanovToGroupKi10()
    {
        TestApp.iGroupController.addStudentToGroup( StudentServiceTestApp.getUuidIvanov() , "KI-10-5" );
    }

    public static void createGroupKI11()
    {
        TestApp.iGroupController.createGroup( "KI-11" );
    }
}
