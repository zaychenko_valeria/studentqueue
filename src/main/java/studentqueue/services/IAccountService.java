package studentqueue.services;

/**
 * Created by lera on 11.03.2016.
 */
public interface IAccountService
{
    void changeAccountFirstName( String uuid , String firstName );
    void changeAccountLastName( String uuid , String lastName );
    void changeAccountEmail( String uuid , String email );
}
