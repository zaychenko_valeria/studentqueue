package studentqueue.services;

/**
 * Created by lera on 17.02.2016.
 */
public interface IInstructorService extends IAccountService
{
    String createInstructor ( String firstName , String lastName , String email );
}
