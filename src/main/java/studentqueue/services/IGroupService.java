package studentqueue.services;

import studentqueue.viewmodel.AcademicGroupBriefView;
import studentqueue.viewmodel.AcademicGroupView;

import java.util.List;
import java.util.UUID;

public interface IGroupService
{
    void createGroup( String groupName );
    void changeGroupName( String groupName , String newGroupName );
    void addStudentToGroup( String uuid , String groupName );
    void removeStudentFromGroup ( String uuid , String groupName );
    void assignCaptain( String uuid , String groupName ) ;
    void deleteGroup( String groupName );

    boolean existGroup( String groupName );
    boolean isInGroup( String uuid , String groupName );
    List< String > getAllStudentsUUIDs( String groupName );

    AcademicGroupView findGroup( String groupName );
    List< AcademicGroupBriefView > viewGroups();
}
