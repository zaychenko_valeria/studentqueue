package studentqueue.services;

import studentqueue.viewmodel.EditDisciplineView;


public interface IDisciplineService
{
    void createDiscipline( String name );
    void createStudyForm( String name , String parentDiscipline , String type );
    void removeStudyForm( String name , String parentDiscipline , String type );
    void createLabAssignment ( String labStudyForm , String parentDiscipline );
    void removeLabAssignment ( String labStudyForm , String parentDiscipline );
    void createTestAssignment ( String parentDiscipline , String testName );
    void removeTestAssignment ( String parentDiscipline , String testName );
    void addLeveltoAssignment ( String parentDiscipline , String levelName ,
                                       int low , int high , String assignmentName );
    void removeLevelFromAssignment ( String parentDiscipline , String assignmentName , String levelName );

    EditDisciplineView queryDiscipline( String parentDiscipline );
}
