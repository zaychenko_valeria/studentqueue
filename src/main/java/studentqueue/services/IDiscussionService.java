package studentqueue.services;

import studentqueue.viewmodel.DiscussionMessageView;

/**
 * Created by lera on 07.03.2016.
 */
public interface IDiscussionService
{
    String createDiscussion( String subject , String uuidOwner , String uuidRecipient , String text );
    void writeMessage( String uuidDiscussion , String text , String uuidAccountSender );
    //List< DiscussionBriefView > getDiscussions( String uuidOwner ); TODO in account services
    DiscussionMessageView viewMessageHistory( String uuidDiscussion );
}
