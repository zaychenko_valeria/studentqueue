package studentqueue.services.impl;

import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.Account;
import studentqueue.repository.IUUIDObjectRepository;
import studentqueue.utils.NonEmptyString;

/**
 * Created by lera on 12.03.2016.
 */
public abstract class AccountService extends GenericService implements studentqueue.services.IAccountService
{

    @Transactional
    @Override
    public void changeAccountFirstName ( String uuid, String firstName )
    {
        Account account = restoreObjectFromRepositopyByUUID( getRepository() , uuid );
        account.setFirstName( new NonEmptyString( firstName ) );

        getRepository().update( account );
    }


    @Transactional
    @Override
    public void changeAccountLastName ( String uuid, String lastName )
    {
        Account account = restoreObjectFromRepositopyByUUID( getRepository() , uuid );
        account.setFirstName( new NonEmptyString( lastName ) );

        getRepository().update( account );
    }


    @Transactional
    @Override
    public void changeAccountEmail ( String uuid, String email )
    {
        Account account = restoreObjectFromRepositopyByUUID( getRepository() , uuid );
        account.setEmail( new NonEmptyString( email ) );

        getRepository().update( account );
    }


    protected abstract IUUIDObjectRepository< Account > getRepository ();

}
