package studentqueue.services.impl;

import studentqueue.repository.INameObjectRepository;
import studentqueue.repository.IUUIDObjectRepository;

/**
 * Created by lera on 17.02.2016.
 */
public abstract class GenericService
{
    public < T > T restoreObjectFromRepositopyByUUID( IUUIDObjectRepository<T> iGenericRepository , String uuid )
    {
        T t = findObjectInRepositopyByUUID( iGenericRepository , uuid );
        if ( t == null )
            throw new RuntimeException( "There isn't such object" );

        return t;
    }

    public < T > T restoreObjectFromRepositopyByName( INameObjectRepository<T> iGenericRepository , String name )
    {
        T t = findObjectInRepositopyByName( iGenericRepository , name );
        if ( t == null )
            throw new RuntimeException( "There isn't such object" );

        return t;
    }


    public < T > boolean restoreOrCreateObjectFromRepositopyByName( INameObjectRepository<T> iGenericRepository , String name )
    {
        T t = iGenericRepository.findObject( name );
        if ( t != null )
            throw new RuntimeException( "You already have object with this name" );

        return true;
    }


    public < T > T findObjectInRepositopyByName( INameObjectRepository<T> iGenericRepository , String name )
    {
        T t = iGenericRepository.findObject( name );
        return t;
    }


    public < T > T findObjectInRepositopyByUUID( IUUIDObjectRepository<T> iGenericRepository , String uuid )
    {
        T t = iGenericRepository.findObject( uuid );
        return t;
    }
}
