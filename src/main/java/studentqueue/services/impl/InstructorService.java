package studentqueue.services.impl;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.Account;
import studentqueue.domain.Instructor;
import studentqueue.repository.IInstructorRepository;
import studentqueue.repository.IUUIDObjectRepository;
import studentqueue.utils.NonEmptyString;

import javax.inject.Inject;

/**
 * Created by lera on 18.02.2016.
 */
@Service
public class InstructorService extends AccountService implements studentqueue.services.IInstructorService
{

    @Transactional
    @Override
    public String createInstructor ( String firstName, String lastName, String email )
    {
        /**
         *  Check accepted values is not null and not empty
         *  Send them to Student constructor
         */
        Instructor instructor = new Instructor(  new NonEmptyString( firstName )
                ,    new NonEmptyString( lastName )
                ,    new NonEmptyString( email )
        );
        getRepository().save( instructor );
        return instructor.getUUID();
    }


    @Override
    protected IUUIDObjectRepository< Account > getRepository ()
    {
        IUUIDObjectRepository repository = iInstructorRepository;
        return repository;
    }


    @Inject
    private IInstructorRepository iInstructorRepository;

}
