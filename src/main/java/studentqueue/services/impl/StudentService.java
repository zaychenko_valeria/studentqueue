package studentqueue.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.Account;
import studentqueue.domain.Student;
import studentqueue.repository.IStudentRepository;
import studentqueue.repository.IUUIDObjectRepository;
import studentqueue.services.impl.viewmodelsimpl.FullStudentViewImpl;
import studentqueue.utils.NonEmptyString;
import studentqueue.viewmodel.FullStudentView;

import javax.inject.Inject;


@Service
public class StudentService extends AccountService implements studentqueue.services.IStudentService
{
    @Transactional
    @Override
    public String createStudent ( String firstName, String lastName, String email )
    {
        /**
         *  Check accepted values is not null and not empty
         *  Send them to Student constructor
         */
        Student student = new Student(  new NonEmptyString( firstName )
                ,    new NonEmptyString( lastName )
                ,    new NonEmptyString( email )
        );
        getRepository().save( student );
        return student.getUUID();
    }


    @Override
    public FullStudentView findStudent ( String studentUuid )
    {
        Student student = restoreObjectFromRepositopyByUUID( iStudentRepository , studentUuid );
        String groupName = iStudentRepository.findGroupContainingStudent( studentUuid );
        return new FullStudentViewImpl( student , groupName );
    }


    @Override
    protected IUUIDObjectRepository< Account > getRepository ()
    {
        IUUIDObjectRepository repository = iStudentRepository;
        return repository;
    }


    @Inject
    private IStudentRepository iStudentRepository;
}
