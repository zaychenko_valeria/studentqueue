package studentqueue.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.*;
import studentqueue.repository.IDisciplineRepository;
import studentqueue.repository.IGroupRepository;
import studentqueue.repository.IInstructorRepository;
import studentqueue.repository.IOfferingRepository;

import javax.inject.Inject;

@Service
public class DisciplineOfferingService extends GenericService implements studentqueue.services.IDisciplineOfferingService
{

//*******    IMPLEMENTED METHODS    ***************************************************

    @Transactional
    @Override
    public String createOffering( String startDateName , String finishDateName
                             , String disciplineName , String formOfControlName , String uuidLecturer )
    {
        LocalDate startDate = getDateFromString( startDateName );
        LocalDate finishDate = getDateFromString( finishDateName );
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , disciplineName );

        FormOfControl formOfControl = FormOfControl.fromString( formOfControlName );
        Instructor instructor = restoreObjectFromRepositopyByUUID( iInstructorRepository , uuidLecturer );

        DisciplineOffering disciplineOffering = new DisciplineOffering( startDate , finishDate , discipline , formOfControl , instructor );
        iOfferingRepository.save( disciplineOffering );
        return disciplineOffering.getUUID();
    }


    @Transactional
    @Override
    public void assignGroupToOffering( String groupName , String uuidOffering )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );

        if ( disciplineOffering.isListeningOffering( academicGroup ) )
            throw new RuntimeException( "This group is already listen offering" );

        disciplineOffering.assignGroupToOffering( academicGroup );
        iOfferingRepository.save( disciplineOffering );
    }


    @Transactional
    @Override
    public void removeGroupFromOffering ( String groupName, String uuidOffering )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );

        disciplineOffering.removeGroupFromOffering( academicGroup );
        iOfferingRepository.save( disciplineOffering );
    }


    @Transactional
    @Override
    public void setFormOfControl( String groupName ,
                          String formOfControlName , String uuidOffering )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );
        FormOfControl formOfControl = FormOfControl.fromString( formOfControlName );

        disciplineOffering.setFormOfControl( formOfControl );
        iOfferingRepository.save( disciplineOffering );
    }


    @Transactional
    @Override
    public void changeDates( String startDateName , String finishDateName , String uuidOffering )
    {
        LocalDate startDate = getDateFromString( startDateName );
        LocalDate finishDate = getDateFromString( finishDateName );
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );

        disciplineOffering.setDate( startDate , finishDate );
        iOfferingRepository.update( disciplineOffering );
    }


    @Transactional
    @Override
    public void addAssistant( String uuidOffering , String uuidInstructor )
    {
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );
        Instructor assistant = restoreObjectFromRepositopyByUUID( iInstructorRepository , uuidInstructor );

        disciplineOffering.addAssistantToOffering( assistant );
        iOfferingRepository.save( disciplineOffering );
    }


    @Transactional
    @Override
    public void changeLecturer ( String uuidOffering, String uuidInstructor )
    {
        DisciplineOffering disciplineOffering = restoreObjectFromRepositopyByUUID( iOfferingRepository , uuidOffering );
        Instructor lecturer = restoreObjectFromRepositopyByUUID( iInstructorRepository , uuidInstructor );

        disciplineOffering.addLecturerToOffering( lecturer );
        iOfferingRepository.update( disciplineOffering );
    }


//*******    PRIVATE HELPER METHODS    ***********************************************


    private LocalDate getDateFromString( String dateName )
    {
        try {
            LocalDate date = LocalDate.parse( dateName , format );
            return date;
        } catch ( DateTimeParseException e) {
            throw new RuntimeException( "Cannot convert date" );
        }
    }


//*******    ATTRIBUTES    ***********************************************************


    private static DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Inject
    private IOfferingRepository iOfferingRepository;

    @Inject
    private IDisciplineRepository iDisciplineRepository;

    @Inject
    private IGroupRepository iGroupRepository;

    @Inject
    private IInstructorRepository iInstructorRepository;
}
