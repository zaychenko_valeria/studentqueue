package studentqueue.services.impl.viewmodelsimpl;

import studentqueue.domain.Student;
import studentqueue.viewmodel.FullStudentView;

/**
 * Created by lera on 31.03.2016.
 */
public class FullStudentViewImpl implements FullStudentView
{
    public FullStudentViewImpl( Student student , String groupName )
    {
        this.student = student;
        this.groupName = groupName;
    }


    @Override
    public String getGroup ()
    {
        return groupName;
    }


    @Override
    public String getFirstName ()
    {
        return student.getFirstName();
    }


    @Override
    public String getLastName ()
    {
        return student.getLastName();
    }


    @Override
    public String getEmail ()
    {
        return student.getEmail();
    }


    @Override
    public String getUUID ()
    {
        return student.getUUID();
    }


    private Student student;
    private String groupName;
}
