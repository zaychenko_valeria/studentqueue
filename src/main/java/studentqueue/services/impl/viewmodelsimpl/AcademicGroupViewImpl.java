package studentqueue.services.impl.viewmodelsimpl;

import studentqueue.domain.AcademicGroup;
import studentqueue.viewmodel.AcademicGroupView;
import studentqueue.viewmodel.AccountView;

import java.util.List;

/**
 * Created by lera on 01.04.2016.
 */
public class AcademicGroupViewImpl implements AcademicGroupView
{
    public AcademicGroupViewImpl( AcademicGroup academicGroup , List< AccountView > briefListOfStudents ,
                                            List< String > disciplineOffering )
    {
        this.academicGroup = academicGroup;
        this.briefListOfStudents = briefListOfStudents;
        this.disciplineOffering = disciplineOffering;
    }


    @Override
    public String getName ()
    {
        return academicGroup.getName();
    }


    @Override
    public List< String > getCurrentDisciplineOffering ()
    {
        return disciplineOffering;
    }


    @Override
    public List< AccountView > getBriefListOfStudents ()
    {
        return briefListOfStudents;
    }


    private AcademicGroup academicGroup;
    private List< AccountView > briefListOfStudents;
    private List< String > disciplineOffering;
}
