package studentqueue.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.*;
import studentqueue.repository.IAccountRepository;
import studentqueue.repository.IDiscussionRepository;
import studentqueue.repository.IInstructorRepository;
import studentqueue.repository.IStudentRepository;
import studentqueue.viewmodel.DiscussionMessageView;

import javax.inject.Inject;
import java.util.Iterator;

/**
 * Created by lera on 07.03.2016.
 */
@Service
public class DiscussionService extends GenericService implements studentqueue.services.IDiscussionService
{


    private void fillMessagesView( Discussion discussion , DiscussionMessageView discussionMessageView )
    {
        Iterator< Message > messageIterator = discussion.getMessagesIterator();

        while( messageIterator.hasNext() )
        {
            Message message = messageIterator.next();
            DiscussionMessageView.MessageView messageView = new DiscussionMessageView.MessageView(
                                                        message.getSender().toString() , message.getText() ,
                                                        message.getPostingDate() );
            discussionMessageView.addMessage( messageView );
        }
    }


    @Override
    public DiscussionMessageView viewMessageHistory( String uuidDiscussion )
    {
        Discussion discussion = restoreObjectFromRepositopyByUUID( iDiscussionRepository , uuidDiscussion );
        DiscussionMessageView discussionMessageView = new DiscussionMessageView(
                                                discussion.getSubject() ,
                                                discussion.getRecipient().toString() );
        fillMessagesView( discussion , discussionMessageView );
        return discussionMessageView;
    }


    @Transactional
    @Override
    public String createDiscussion ( String subject, String uuidOwner, String uuidRecipient, String text )
    {
        Student owner = restoreObjectFromRepositopyByUUID( iStudentRepository , uuidOwner );
        Instructor recipient = restoreObjectFromRepositopyByUUID( iInstructorRepository , uuidRecipient );
        Discussion discussion = new Discussion( subject , owner , recipient , new Message( text , owner ) );
        iDiscussionRepository.save( discussion );
        return discussion.getUuid();
    }


    @Transactional
    @Override
    public void writeMessage ( String uuidDiscussion , String text , String uuidAccountSender )
    {
        Account account = iStudentRepository.findAccount( uuidAccountSender );
        if ( account == null )
            account = iInstructorRepository.findAccount( uuidAccountSender );
        if ( account == null )
            throw new RuntimeException( "Account unresolved" );

        Discussion discussion = restoreObjectFromRepositopyByUUID( iDiscussionRepository , uuidDiscussion );
        discussion.addMessage( new Message( text , account ) );
        iDiscussionRepository.update( discussion );
    }


    @Inject
    private IDiscussionRepository iDiscussionRepository;

    @Inject
    private IStudentRepository iStudentRepository;

    @Inject
    private IInstructorRepository iInstructorRepository;
}
