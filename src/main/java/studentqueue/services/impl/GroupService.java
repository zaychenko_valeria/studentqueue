package studentqueue.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.AcademicGroup;
import studentqueue.domain.Account;
import studentqueue.domain.Student;
import studentqueue.repository.IGroupRepository;
import studentqueue.repository.IStudentRepository;
import studentqueue.services.impl.viewmodelsimpl.AcademicGroupViewImpl;
import studentqueue.utils.NonEmptyString;
import studentqueue.viewmodel.AcademicGroupBriefView;
import studentqueue.viewmodel.AcademicGroupView;
import studentqueue.viewmodel.AccountView;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GroupService extends GenericService implements studentqueue.services.IGroupService
{

//*******    IMPLEMENTED METHODS    **************************************************


    @Transactional
    @Override
    public void createGroup( String groupName )
    {
        if ( restoreOrCreateObjectFromRepositopyByName( iGroupRepository , groupName ) )
        {
            AcademicGroup academicGroup = new AcademicGroup( new NonEmptyString( groupName ) );
            iGroupRepository.save( academicGroup );
        }
    }


    @Transactional
    @Override
    public void changeGroupName ( String groupName, String newGroupName )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        academicGroup.setName( new NonEmptyString( newGroupName ) );
        iGroupRepository.update( academicGroup );
    }


    @Transactional
    @Override
    public void addStudentToGroup( String uuid , String groupName )
    {
        Student student = restoreObjectFromRepositopyByUUID( iStudentRepository , uuid );
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        //TODO студент не должен быть в другой группе в данный момент или удалять его при переносе

        if ( /*!*/ academicGroup.isInGroup( student ) )
            throw new RuntimeException ( "This student is already in group" );

        academicGroup.addStudent( student );
        iGroupRepository.save( academicGroup );
    }


    @Transactional
    @Override
    public void removeStudentFromGroup ( String uuid, String groupName )
    {
        Student student = restoreObjectFromRepositopyByUUID( iStudentRepository , uuid );
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );

        academicGroup.removeStudentFromGroup( student );
        iGroupRepository.save( academicGroup );
    }


    @Transactional
    @Override
    public void assignCaptain( String uuid , String groupName )
    {
        Student student = restoreObjectFromRepositopyByUUID( iStudentRepository , uuid );
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );

        if ( ! academicGroup.isInGroup( student ) )
            throw new RuntimeException ( "This student isn`t in group" );

        academicGroup.assignCaptain( student );
        iGroupRepository.save( academicGroup );
    }


    @Transactional
    @Override
    public List< String > getAllStudentsUUIDs( String groupName )
    {
        List< String > allUuids = new ArrayList< String >();
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        Iterator< Student > iterator= academicGroup.getIterator();

        while( iterator.hasNext() )
        {
            Student student = iterator.next();
            allUuids.add( student.getUUID() );
        }

        return allUuids;
    }


    @Transactional
    @Override
    public boolean existGroup( String groupName )
    {
        return ( findObjectInRepositopyByName( iGroupRepository , groupName ) != null );
    }


    @Transactional
    @Override
    public boolean isInGroup( String uuid , String groupName )
    {
        Student student = restoreObjectFromRepositopyByUUID( iStudentRepository , uuid );
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );

        return academicGroup.isInGroup( student );
    }


    @Transactional
    @Override
    public void deleteGroup( String groupName )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );
        iGroupRepository.delete( academicGroup );
    }


//*******    VIEWMODEL IMPLEMENTED METHODS    ****************************************


    @Transactional
    @Override
    public AcademicGroupView findGroup( String groupName )
    {
        AcademicGroup academicGroup = restoreObjectFromRepositopyByName( iGroupRepository , groupName );

        List< AccountView > briefStudentsView = new ArrayList< AccountView >(  );
        Iterator< Student > studentIterator = academicGroup.getIterator();
        while ( studentIterator.hasNext() )
            briefStudentsView.add( studentIterator.next() );

        List< String > offerings = iGroupRepository.findOfferingContainingGroup( groupName );
        return new AcademicGroupViewImpl( academicGroup , briefStudentsView , offerings );
        //HQL REPOSITORY OFFERing
    }


    @Transactional
    @Override
    public List< AcademicGroupBriefView > viewGroups()
    {
        List< AcademicGroupBriefView > academicGroupBriefViews = new ArrayList<>();
        List< AcademicGroup > l = iGroupRepository.loadAll().stream()
                .sorted( ( AcademicGroup g1, AcademicGroup g2 ) -> g1.getName().compareTo( g2.getName() ) )
                .collect( Collectors.toList() );

        for( AcademicGroup group : l )
        {
            academicGroupBriefViews.add( new AcademicGroupViewImpl( group , null , null ));
        }
        return academicGroupBriefViews;
    }


//*******    ATTRIBUTES    ***********************************************************


    @Inject
    private IGroupRepository iGroupRepository;

    @Inject
    private IStudentRepository iStudentRepository;
}
