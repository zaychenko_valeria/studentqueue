package studentqueue.services.impl;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.domain.*;
import studentqueue.repository.IDisciplineRepository;
import studentqueue.utils.NonEmptyString;
import studentqueue.viewmodel.EditDisciplineView;
import studentqueue.viewmodel.StudyFormView;
import studentqueue.viewmodel.ScoringRulesView;
import studentqueue.viewmodel.ScoringRulesView.AssignmentScoringRules;
import studentqueue.viewmodel.StudyFormView.StudyFormViewRule;

import javax.inject.Inject;

@Service
public class DisciplineService extends GenericService implements studentqueue.services.IDisciplineService
{


//*******    SCORING RULES QUERY METHODS    ******************************************


    private void fillScoringRulesView(  Discipline discipline ,
                                                   ScoringRulesView view )
    {
        for ( int i = 0 ; i < discipline.getAssignmentsCount() ; i ++ )
        {
            Assignment iAssignment = discipline.getAssignment( i );
            view.addCheckPointRule( makeAssignmentScoringRules( iAssignment ) );
        }
    }


    private AssignmentScoringRules makeAssignmentScoringRules ( Assignment assignment )
    {
        AssignmentScoringRules assignmentScoringRule =
            new AssignmentScoringRules(
                LevelEnum.values().length);


        for ( int k = 0; k < LevelEnum.values().length; k++ )
        {
            AssignmentRange r = assignment.getRangeOfLevel(LevelEnum.values()[k]);
            int low = ( r != null )? r.getLow() : 0;
            int high = ( r != null )? r.getHigh() : 0;
            assignmentScoringRule.setColumn( k , low , high );
        }

        return assignmentScoringRule;
    }

//*******    FORM OF CONTROL QUERY METHODS    ****************************************


    private void fillFormsOfControlView ( Discipline discipline, StudyFormView view )
    {
        int studyFormQuantity = discipline.getStudyFormsCount();

        for ( int i = 0; i < studyFormQuantity; i++ )
        {
            StudyForm studyForm = discipline.getStudyForms( i );
            StudyFormViewRule studyFormViewRule = new StudyFormViewRule(
                    studyFormQuantity , studyForm.getName() );
            view.addStudyFormRule(studyFormViewRule);
        }
    }

//*******    DISCIPLINE QUERY METHODS    *********************************************


    @Override
    public EditDisciplineView queryDiscipline( String parentDiscipline )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );

        EditDisciplineView editDisciplineView = new EditDisciplineView( parentDiscipline );
        fillScoringRulesView( discipline, editDisciplineView.getScoringRulesView() );
        fillFormsOfControlView( discipline, editDisciplineView.getStudyFormView() );
        return editDisciplineView;
    }


//*******    IMPLEMENTED METHODS    **************************************************

    @Transactional
    @Override
    public void createDiscipline( String disciplineName )
    {
        if ( restoreOrCreateObjectFromRepositopyByName( iDisciplineRepository , disciplineName ) )
        {
            Discipline discipline = new Discipline( new NonEmptyString( disciplineName ) );
            iDisciplineRepository.save( discipline );
        }
    }


    @Transactional
    @Override
    public void createStudyForm( String name , String parentDiscipline , String type )
    {
        StudyFormType studyFormType = StudyFormType.fromString( type );
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );

        StudyForm studyForm = discipline.findStudyForm( name , studyFormType );
        if ( studyForm != null )
            throw new RuntimeException( "This discipline already has study form " + name );

        studyForm = new StudyForm( new NonEmptyString( name ) , studyFormType );
        discipline.addStudyForm( studyForm );
        iDisciplineRepository.save( discipline );
    }


    @Transactional
    @Override
    public void removeStudyForm ( String name, String parentDiscipline , String type )
    {
        StudyFormType studyFormType = StudyFormType.fromString( type );
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );

        StudyForm studyForm = resolveStudyForm( discipline , name , studyFormType );
        discipline.removeStudyForm( studyForm );
        iDisciplineRepository.save( discipline );
    }


    @Transactional
    @Override
    public void createLabAssignment ( String studyFormName , String parentDiscipline )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );
        StudyForm studyFormObject = resolveStudyForm( discipline , studyFormName , StudyFormType.LAB_TYPE );
        Assignment labAssignment = discipline.findAssignment( studyFormObject.getName() );

        if ( labAssignment != null )
            throw new RuntimeException( "This discipline already has lab point" + studyFormName );

        labAssignment = new LabAssignment( studyFormObject );
        discipline.addAssignment( labAssignment );
        iDisciplineRepository.save( discipline );
    }


    @Transactional
    @Override
    public void removeLabAssignment ( String labStudyForm, String parentDiscipline )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );
        StudyForm studyFormObject = resolveStudyForm( discipline , labStudyForm , StudyFormType.LAB_TYPE );
        Assignment labAssignment = discipline.findAssignment( studyFormObject.getName() );

        removeAssignment( discipline , labAssignment );
    }


    @Transactional
    @Override
    public void createTestAssignment ( String parentDiscipline , String testName )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );
        Assignment testAssignment = discipline.findAssignment( testName );

        if ( testAssignment != null )
            throw new RuntimeException( "This discipline already has test point" + testName );

        testAssignment = new TestAssignment( new NonEmptyString( testName ) );
        discipline.addAssignment( testAssignment );
        iDisciplineRepository.save( discipline );
    }


    @Transactional
    @Override
    public void removeTestAssignment ( String parentDiscipline, String testName )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );
        Assignment testAssignment = discipline.findAssignment( testName );
        removeAssignment( discipline , testAssignment );
    }


    @Transactional
    @Override
    public void addLeveltoAssignment ( String parentDiscipline , String levelName ,
                                       int low , int high , String assignmentName )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );
        Assignment assignment = discipline.findAssignment( assignmentName );

        if ( assignment == null )
            throw new RuntimeException( "This discipline doesn`t have test point" + assignmentName );

        LevelEnum value = LevelEnum.fromString( levelName );
        AssignmentRange range = assignment.getRangeOfLevel( value );

        if ( range != null)
            throw new RuntimeException( "This discipline " + parentDiscipline +
                                        " and test point " + assignmentName +
                                        " already has level" + levelName );

        assignment.addLevelToAssignment( value , new AssignmentRange( low , high ) );
        iDisciplineRepository.save( discipline );
    }


    @Transactional
    @Override
    public void removeLevelFromAssignment ( String parentDiscipline, String assignmentName , String levelName )
    {
        Discipline discipline = restoreObjectFromRepositopyByName( iDisciplineRepository , parentDiscipline );

        Assignment assignment = discipline.findAssignment( assignmentName );

        if ( assignment == null )
            throw new RuntimeException( "This discipline doesn`t have test point" + assignmentName );

        LevelEnum value = LevelEnum.fromString( levelName );

        assignment.removeLevelFromAssignment( value );
        iDisciplineRepository.save( discipline );
    }


// *******    HELPER METHODS    ******************************************************


    private void removeAssignment( Discipline discipline , Assignment assignment )
    {
        if ( assignment == null )
            throw new RuntimeException( "This discipline  hasn't lab point" );

        discipline.removeAssignment( assignment );
        iDisciplineRepository.save( discipline );
    }


    private StudyForm resolveStudyForm ( Discipline d, String name, StudyFormType type )
    {
        StudyForm f = d.findStudyForm( name , type );
        if ( f == null )
            throw new RuntimeException( "Required study form " + name + " was not found" );

        return f;
    }


//*******    ATTRIBUTES    ***********************************************************


    @Inject
    private IDisciplineRepository iDisciplineRepository;
}
