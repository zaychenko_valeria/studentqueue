package studentqueue.services;

import studentqueue.viewmodel.FullStudentView;

public interface IStudentService extends IAccountService
{
    String createStudent ( String firstName , String lastName , String email );
    FullStudentView findStudent ( String studentUuid );
}
