package studentqueue.services;


public interface IDisciplineOfferingService
{
    String createOffering( String startDate , String finishDate ,
                    String disciplineName , String formOfControlName , String uuidLecturer );
    void assignGroupToOffering( String groupName , String uuidOffering );
    void removeGroupFromOffering ( String groupName , String uuidOffering );
    void setFormOfControl( String groupName , String formOfControlName , String uuidOffering );
    void changeDates( String startDate , String finishDate , String uuidOffering );
    void addAssistant( String uuidOffering , String uuidInstructor );
    void changeLecturer( String uuidOffering , String uuidInstructor );
}
