package studentqueue.repository;

import studentqueue.domain.Discipline;

/**
 * Created by lera on 30.01.2016.
 */
public interface IDisciplineRepository extends INameObjectRepository< Discipline >
{
}
