package studentqueue.repository;

import studentqueue.domain.Discussion;

/**
 * Created by lera on 08.03.2016.
 */
public interface IDiscussionRepository extends IUUIDObjectRepository< Discussion >
{
}
