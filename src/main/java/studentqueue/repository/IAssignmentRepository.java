package studentqueue.repository;

import studentqueue.domain.Assignment;

/**
 * Created by lera on 10.12.2015.
 */
public interface IAssignmentRepository extends IGenericRepository< Assignment >
{ }
