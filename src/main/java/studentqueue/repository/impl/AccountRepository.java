package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Account;
import studentqueue.repository.IAccountRepository;

/**
 * Created by lera on 12.03.2016.
 */
public class AccountRepository< T extends Account >
        extends UUIDObjectRepository< T > implements IAccountRepository< T >
{
    public AccountRepository( Class dataClass , String tableName )
    {
        super( dataClass, tableName );
    }

    @Override
    public Account findAccount ( String uuid )
        {
            Account account = ( Account ) entityManager.createQuery( "from "  + "Account" + " x " +
                    "where x.uuid = '" + uuid + "'" )
                    .getSingleResult();
            return account;
        }

}
