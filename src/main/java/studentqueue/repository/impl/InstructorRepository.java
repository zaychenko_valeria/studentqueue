package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Instructor;
import studentqueue.repository.IInstructorRepository;

/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class InstructorRepository extends AccountRepository< Instructor > implements IInstructorRepository
{
    public InstructorRepository()
    {
        super( Instructor.class, "Instructor" );
    }

}
