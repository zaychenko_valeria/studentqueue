package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.StudyForm;
import studentqueue.repository.IStudyFormRepository;

/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class StudyFormRepository extends GenericRepository< StudyForm > implements IStudyFormRepository
{
    public StudyFormRepository()
    {
        super( StudyForm.class );
    }
}
