package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Discipline;
import studentqueue.repository.IDisciplineRepository;

/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class DisciplineRepository extends NameObjectRepository< Discipline > implements IDisciplineRepository
{
    public DisciplineRepository()
    {
        super( Discipline.class , "Discipline" );
    }
}
