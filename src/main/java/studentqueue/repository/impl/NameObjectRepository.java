package studentqueue.repository.impl;

import studentqueue.repository.INameObjectRepository;

import java.util.List;

/**
 * Created by lera on 18.02.2016.
 */
public class NameObjectRepository< T > extends GenericRepository< T > implements INameObjectRepository< T >
{
    protected NameObjectRepository( Class dataClass , String tableName )
    {
        super( dataClass );
        this.tableName = tableName;
    }


    @Override
    public T findObject ( String name )
    {
        List< T > results = entityManager.createQuery( "from "  + tableName + " x " +
                "where x.name = '" + name + "'" ).getResultList();

        if ( results.isEmpty() )
            return null;
        return ( T ) results.get( 0 );
    }

    private String tableName;
}
