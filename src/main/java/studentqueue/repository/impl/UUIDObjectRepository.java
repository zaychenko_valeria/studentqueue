package studentqueue.repository.impl;

import studentqueue.repository.IUUIDObjectRepository;

/**
 * Created by lera on 17.02.2016.
 */
public abstract class UUIDObjectRepository< T > extends GenericRepository< T > implements IUUIDObjectRepository< T >
{
    protected UUIDObjectRepository( Class dataClass , String tableName )
    {
        super( dataClass );
        this.tableName = tableName;
    }


    @Override
    public T findObject ( String uuid )
    {
        T t = ( T ) entityManager.createQuery( "from "  + tableName + " x " +
                "where x.uuid = '" + uuid + "'" )
                .getSingleResult();
        return t;
    }

    private String tableName;
}
