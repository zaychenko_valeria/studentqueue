package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Student;
import studentqueue.repository.IStudentRepository;


/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class StudentRepository extends AccountRepository< Student > implements IStudentRepository
{
    public StudentRepository()
    {
        super( Student.class, "Student" );
    }


    public String findGroupContainingStudent ( String uuidStudent )
    {
        String groupName = entityManager.createQuery( "from AcademicGroup agroup join agroup.students student "  +
                "where student.uuid = '" + uuidStudent + "'" ).getSingleResult().toString();
        return groupName;
    }
}
