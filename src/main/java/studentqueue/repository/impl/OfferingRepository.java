package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.DisciplineOffering;
import studentqueue.repository.IOfferingRepository;

/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class OfferingRepository extends UUIDObjectRepository< DisciplineOffering > implements IOfferingRepository
{
    public OfferingRepository()
    {
        super( DisciplineOffering.class , "DisciplineOffering" );
    }
}
