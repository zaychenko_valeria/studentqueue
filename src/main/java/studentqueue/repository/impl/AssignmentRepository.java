package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Assignment;
import studentqueue.repository.IAssignmentRepository;

/**
 * Created by lera on 31.01.2016.
 */
@Repository
public class AssignmentRepository extends GenericRepository< Assignment > implements IAssignmentRepository
{
    public AssignmentRepository()
{
    super( Assignment.class );
}
}
