package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.AcademicGroup;
import studentqueue.repository.IGroupRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;
import java.util.List;

/**
 * Created by lera on 28.01.2016.
 */
@Repository
public class GroupRepository extends NameObjectRepository< AcademicGroup > implements IGroupRepository
{
    @PersistenceContext
    protected EntityManager entityManager;

    public GroupRepository()
    {
        super(  AcademicGroup.class , "AcademicGroup" );
    }


    @Override
    public List< String > findOfferingContainingGroup( String groupName )
    {
        List< String > offeringNames = entityManager.createQuery( "from DisciplineOffering offering join offering.groupsOfDiscipline g "  +
                "where g.name = '" + groupName + "'" ).getResultList();
        return offeringNames;
    }


    @Override
    public Collection< AcademicGroup > loadAll()
    {
        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery< AcademicGroup > query = builder.createQuery( AcademicGroup.class );

        return this.entityManager.createQuery(
                query.select( query.from( AcademicGroup.class ) )
        ).getResultList();
    }
}
