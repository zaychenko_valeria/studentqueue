package studentqueue.repository.impl;

import org.springframework.stereotype.Repository;
import studentqueue.domain.Discussion;
import studentqueue.domain.Message;
import studentqueue.repository.IDiscussionRepository;

import java.util.Iterator;
import java.util.List;

/**
 * Created by lera on 08.03.2016.
 */
@Repository
public class DiscussionRepository extends UUIDObjectRepository< Discussion > implements IDiscussionRepository
{
    public DiscussionRepository()
    {
        super(  Discussion.class , "Discussion" );
    }


    public List< Message > loadMessages( String uuid )
    {
        List< Message > result = entityManager.createQuery( "from Discussion discussion join discussion.messages message "  +
                "where discussion.uuid = '" + uuid + "'" ).getResultList();
        return result;
    }
}
