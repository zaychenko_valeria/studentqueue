package studentqueue.repository.impl;

import studentqueue.repository.IGenericRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by lera on 31.01.2016.
 */
public abstract class GenericRepository< T > implements IGenericRepository< T >
{

    protected GenericRepository( Class dataClass )
    {
        this.dataClass = dataClass;
    }


    @Override
    public void save ( T t )
    {
        entityManager.persist( t );
    }


    @Override
    public void update( T t )
    {
        entityManager.merge( t );
    }


    @Override
    public T load ( long id )
    {
        return ( T ) entityManager.find( dataClass , id );
    }


    @Override
    public void delete( T t )
    {
        entityManager.remove( t );
    }

    @PersistenceContext
    protected EntityManager entityManager;

    private Class dataClass;
}
