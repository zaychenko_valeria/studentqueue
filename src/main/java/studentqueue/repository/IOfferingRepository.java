package studentqueue.repository;

import studentqueue.domain.DisciplineOffering;

/**
 * Created by lera on 30.01.2016.
 */
public interface IOfferingRepository extends IUUIDObjectRepository< DisciplineOffering >
{
}
