package studentqueue.repository;

import studentqueue.domain.Account;

/**
 * Created by lera on 12.03.2016.
 */
public interface IAccountRepository< T extends Account > extends IUUIDObjectRepository< T >
{
    Account findAccount ( String uuid );
}
