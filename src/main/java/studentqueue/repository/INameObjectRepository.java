package studentqueue.repository;

/**
 * Created by lera on 18.02.2016.
 */
public interface INameObjectRepository< T > extends IGenericRepository< T >
{
    T findObject( String name );
}
