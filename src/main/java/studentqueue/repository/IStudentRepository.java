package studentqueue.repository;

import studentqueue.domain.Student;

public interface IStudentRepository extends IAccountRepository< Student >
{
    String findGroupContainingStudent ( String uuidStudent );
}
