package studentqueue.repository;

/**
 * Created by lera on 17.02.2016.
 */
public interface IUUIDObjectRepository< T > extends IGenericRepository< T >
{
    T findObject( String uuid );
}
