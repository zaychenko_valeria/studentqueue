package studentqueue.repository;

import studentqueue.domain.StudyForm;

/**
 * Created by lera on 31.01.2016.
 */
public interface IStudyFormRepository extends IGenericRepository< StudyForm >
{
}
