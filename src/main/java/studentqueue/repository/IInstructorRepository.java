package studentqueue.repository;

import studentqueue.domain.Instructor;

/**
 * Created by lera on 31.01.2016.
 */
public interface IInstructorRepository extends IAccountRepository< Instructor >
{
}
