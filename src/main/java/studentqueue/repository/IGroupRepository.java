package studentqueue.repository;

import studentqueue.domain.AcademicGroup;

import java.util.Collection;
import java.util.List;

/**
 * Created by lera on 08.12.2015.
 */
public interface IGroupRepository extends INameObjectRepository< AcademicGroup >
{
    List< String > findOfferingContainingGroup( String groupName );
    Collection < AcademicGroup > loadAll();
}
