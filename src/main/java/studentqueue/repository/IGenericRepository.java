package studentqueue.repository;

/**
 * Created by lera on 30.01.2016.
 */
public interface IGenericRepository< T >
{
    void save( T t );
    T load( long id );
    void update( T t );
    void delete( T t );
}
