package studentqueue.restcontroller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import studentqueue.services.IGroupService;
import studentqueue.viewmodel.AcademicGroupBriefView;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Valeria Zaychenko on 05.07.2016.
 */
@RestController
public class AcademicGroupRestController
{
    @RequestMapping( value = "rest/groups" , method = GET )
    public Collection< String > groups()
    {
        List< String > list = new ArrayList<>();
        for ( AcademicGroupBriefView view : groupService.viewGroups() )
            list.add( view.getName() );

        return list;
    }

    @RequestMapping(value = "rest/groups" , method = RequestMethod.POST)
    public ResponseEntity< Void > createGroup( @RequestBody BriefGroup group )
    {
        groupService.createGroup( group.getName() );
        return new ResponseEntity< Void >( HttpStatus.CREATED);
    }

    public static class BriefGroup
    {
        private String name;

        public String getName() {  return name;   }
        public void setName( String name )  {  this.name = name;   }
    }

        @Inject
    private IGroupService groupService;
}
