package studentqueue.viewmodel;

public class EditDisciplineView {


    public EditDisciplineView( String disciplineName )
    {
        this.disciplineName = disciplineName;
        scoringRulesView = new ScoringRulesView();
        studyFormView = new StudyFormView();
    }


    public String getDisciplineViewName()
    {
        return disciplineName;
    }


    public ScoringRulesView getScoringRulesView()
    {
        return scoringRulesView;
    }


    public StudyFormView getStudyFormView()
    {
        return studyFormView;
    }


    private ScoringRulesView scoringRulesView;
    private StudyFormView studyFormView;
    private String disciplineName;


}
