package studentqueue.viewmodel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ScoringRulesView {

//******    CONSTRUCTOR    ******************************************************************


    public ScoringRulesView()
    {
        this.assignmentRules = new ArrayList< AssignmentScoringRules >();
    }


//******    PUBLIC METHODS    **************************************************************


    public Iterator< AssignmentScoringRules > getCheckPointRuleIterator()
    {
        return this.assignmentRules.listIterator();
    }


    public void addCheckPointRule( AssignmentScoringRules AssignmentRule )
    {
        this.assignmentRules.add( AssignmentRule );
    }


//******    ATTRIBUTES    *******************************************************************


    private List< AssignmentScoringRules > assignmentRules;


//******    NESTED CLASS    *****************************************************************


    public static class AssignmentScoringRules
    {

//******   NESTED CLASS PUBLIC METHODS    ***************************************************


        public int getLowRange( int column )
        {
            return points[ column ][ 0 ];
        }


        public int getHighRange ( int column )
        {
            return points[ column ][ 1 ];
        }


        public int getColumnsCount()
        {
            return points.length;
        }


        public void setColumn ( int column, int low, int high )
        {
            this.points[ column ][ 0 ] =  low ;
            this.points[ column ][ 1 ] =  high ;
        }


//******   NESTED CLASS CONSTRUCTOR    ******************************************************


        public AssignmentScoringRules( int columns )
        {
            points = new int[ columns ][ 2 ];
        }


//******   NESTED CLASS ATTRIBUTES    *******************************************************


        int[][] points;

    }
}
