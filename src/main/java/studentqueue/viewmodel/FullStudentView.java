package studentqueue.viewmodel;

/**
 * Created by lera on 29.03.2016.
 */
public interface FullStudentView extends AccountView
{
    String getGroup ();
    //List < DisciplineOfferingView > getDisciplineOfferings(); TODO
    //List < NotificationView > getNotifications(); TODO
}
