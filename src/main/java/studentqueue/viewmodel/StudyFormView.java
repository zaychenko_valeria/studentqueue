package studentqueue.viewmodel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class StudyFormView
{
//******    CONSTRUCTOR    *****************************************************************

    public StudyFormView()
    {
        this.studyFormViewRules = new ArrayList< StudyFormViewRule >();
    }

//******    PUBLIC METHODS    **************************************************************

    public Iterator< StudyFormViewRule > getStudyFormViewIterator()
    {
        return this.studyFormViewRules.listIterator();
    }


    public void addStudyFormRule( StudyFormViewRule studyFormViewRule )
    {
        this.studyFormViewRules.add( studyFormViewRule );
    }

//******    ATTRIBUTES    ******************************************************************

    private List< StudyFormViewRule > studyFormViewRules;


//******    NESTED CLASS    ****************************************************************


    public static class StudyFormViewRule
    {
    //******   NESTED CLASS CONSTRUCTOR    ******************************************************

        public StudyFormViewRule( int quantity , String name )
        {
            studyFormQuantity = quantity;
            studyFormName = name;
        }

    //******   NESTED CLASS PUBLIC METHODS    ***************************************************

        public int getStudyFormQuantity()
        {
            return studyFormQuantity;
        }


        public String getStudyFormName()
        {
            return studyFormName;
        }



    //******   NESTED CLASS ATTRIBUTES    *******************************************************

        private int studyFormQuantity;
        private String studyFormName;
    }
}
