package studentqueue.viewmodel;

/**
 * Created by lera on 29.03.2016.
 */
public interface AccountView
{
    String getFirstName ();
    String getLastName ();
    String getEmail ();
    String getUUID ();
}
