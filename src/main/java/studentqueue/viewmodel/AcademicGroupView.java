package studentqueue.viewmodel;

import java.util.List;

/**
 * Created by lera on 29.03.2016.
 */
public interface AcademicGroupView extends AcademicGroupBriefView
{
    List< String > getCurrentDisciplineOffering();
    List< AccountView > getBriefListOfStudents();
}
