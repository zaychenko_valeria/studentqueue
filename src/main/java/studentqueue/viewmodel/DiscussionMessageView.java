package studentqueue.viewmodel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lera on 11.03.2016.
 */
public class DiscussionMessageView
{
//******    CONSTRUCTOR    *****************************************************************

    public DiscussionMessageView( String subject , String recipient )
    {
        this.subject = subject;
        this.recipient = recipient;
        messageViews = new ArrayList< MessageView >();
    }

//******    PUBLIC METHODS    **************************************************************

    public String getSubject ()
    {
        return subject;
    }


    public String getRecipient ()
    {
        return recipient;
    }


    public Iterator< MessageView > getMessageViewsIterator()
    {
        return messageViews.listIterator();
    }


    public void addMessage( MessageView messageView )
    {
        messageViews.add( messageView );
    }

//******    ATTRIBUTES    ******************************************************************

    String subject;
    String recipient;
    List< MessageView > messageViews;


//******    NESTED CLASS    ****************************************************************


    public static class MessageView
    {
    //******   NESTED CLASS CONSTRUCTOR    ******************************************************

        public MessageView ( String sender, String text, long arrivedAt )
        {
            this.sender = sender;
            this.text = text;
            this.arrivedAt = arrivedAt;
        }

    //******   NESTED CLASS PUBLIC METHODS    ***************************************************

        public String getSender ()
        {
            return sender;
        }


        public String getText ()
        {
            return text;
        }


        public long getArrivedAt ()
        {
            return arrivedAt;
        }

    //******   NESTED CLASS ATTRIBUTES    *******************************************************

        String sender;
        String text;
        long arrivedAt;
    }
}
