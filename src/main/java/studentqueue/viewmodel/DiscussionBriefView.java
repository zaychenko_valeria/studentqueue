package studentqueue.viewmodel;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by lera on 10.03.2016.
 */

public class DiscussionBriefView
{
    public DiscussionBriefView( String owner , String recipient , String subject , LocalDateTime createDateTime )
    {
        this.owner = owner;
        this.recipient = recipient;
        this.subject = subject;
        this.createDateTime = createDateTime;
    }

    public String getOwner ()
    {
        return owner;
    }

    public String getRecipient ()
    {
        return recipient;
    }

    public String getSubject ()
    {
        return subject;
    }

    public LocalDateTime getCreateTime ()
    {
        return createDateTime;
    }

    String owner;
    String recipient;
    String subject;
    //String lastMessage; TODO add
    LocalDateTime createDateTime;
}
