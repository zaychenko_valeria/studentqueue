package studentqueue.viewmodel;

/**
 * Created by lera on 14.04.2016.
 */
public interface AcademicGroupBriefView
{
    String getName();
}
