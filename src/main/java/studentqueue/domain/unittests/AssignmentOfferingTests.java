package studentqueue.domain.unittests;

import org.junit.Test;
import static org.junit.Assert.*;
import studentqueue.domain.*;
import studentqueue.utils.NonEmptyString;

import java.time.LocalDate;

/**
 * Created by lera on 21.02.2016.
 */
public class AssignmentOfferingTests
{
    @Test
    public void сonstructorCreatesAssignmentOfferingProperly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm sf_lab1 = new StudyForm( new NonEmptyString( "lab1" ) , StudyFormType.LAB_TYPE );
        Assignment a_lab1A = new LabAssignment( sf_lab1 );
        DisciplineOffering disciplineOffering = new DisciplineOffering( LocalDate.of( 2016 , 02 , 21 ) ,
                                                                    LocalDate.of( 2016 , 10 , 13 ) ,
                                                    ipz , FormOfControl.COMBINED_EXAMINATION ,
                                                    null );
        AssignmentOffering lab2015 = new AssignmentOffering( a_lab1A , disciplineOffering );

        assertNotNull( lab2015.getUuid() );
    }


    @Test
    public void setStartDateProperly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm sf_lab1 = new StudyForm( new NonEmptyString( "lab1" ) , StudyFormType.LAB_TYPE );
        Assignment a_lab1A = new LabAssignment( sf_lab1 );
        DisciplineOffering disciplineOffering = new DisciplineOffering( LocalDate.of( 2016 , 02 , 21 ) ,
                LocalDate.of( 2016 , 10 , 13 ) ,
                ipz , FormOfControl.COMBINED_EXAMINATION ,
                null );
        AssignmentOffering lab2015 = new AssignmentOffering( a_lab1A , disciplineOffering );
        LocalDate startDate = LocalDate.of( 2016 , 04 , 10 );
        lab2015.setStartDate( startDate );
        assertEquals( lab2015.getStartDate() , startDate );
    }


    @Test
    public void setFinishDateProperly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm sf_lab1 = new StudyForm( new NonEmptyString( "lab1" ) , StudyFormType.LAB_TYPE );
        Assignment a_lab1A = new LabAssignment( sf_lab1 );
        DisciplineOffering disciplineOffering = new DisciplineOffering( LocalDate.of( 2016 , 02 , 21 ) ,
                LocalDate.of( 2016 , 10 , 13 ) ,
                ipz , FormOfControl.COMBINED_EXAMINATION ,
                null );
        AssignmentOffering lab2015 = new AssignmentOffering( a_lab1A , disciplineOffering );
        LocalDate finishDate = LocalDate.of( 2016 , 05 , 10 );
        lab2015.setStartDate( finishDate );
        assertEquals( lab2015.getStartDate() , finishDate );
    }
}
