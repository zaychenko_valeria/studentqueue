package studentqueue.domain.unittests;

import org.junit.Test;
import studentqueue.domain.Discussion;
import studentqueue.domain.Instructor;
import studentqueue.domain.Message;
import studentqueue.domain.Student;
import studentqueue.utils.NonEmptyString;

import static org.junit.Assert.*;

/**
 * Created by lera on 06.03.2016.
 */
public class DiscussionTests
{
    @Test
    public void constructorInitsCorrectly()
    {
        String subject = "IPZ";
        Student student = new Student(
                new NonEmptyString( "Ihor" ) ,
                new NonEmptyString( "Lastochkin" ) ,
                new NonEmptyString( "il!ya.ru" ) );
        Instructor instructor = new Instructor(
                new NonEmptyString( "Sergei" ) ,
                new NonEmptyString( "Zaychenko" ) ,
                new NonEmptyString( "ddd@ru" ) );
        Message message = new Message( "first message to lector" , student );
        Discussion discussion = new Discussion( subject , student , instructor , message );

        assertEquals( discussion.getOwner() , student );
        assertEquals( discussion.getRecipient() , instructor );
        assertEquals( discussion.getSubject() , subject );
        assertEquals( discussion.getMessagesCount() , 1 );
        assertNotNull( discussion.getUuid() );
    }


    @Test
    public void addMessageCorrectly()
    {
        String subject = "IPZ";
        Student student = new Student(
                new NonEmptyString( "Ihor" ) ,
                new NonEmptyString( "Lastochkin" ) ,
                new NonEmptyString( "il!ya.ru" ) );
        Message message = new Message( "first message to lector" , student );
        Discussion discussion = new Discussion( subject , student , null , message );
        discussion.addMessage( new Message( "new message" , student) );
    }

    //TODO check owner recipient not null
}
