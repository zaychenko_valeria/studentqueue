package studentqueue.domain.unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import studentqueue.domain.*;
import studentqueue.utils.NonEmptyString;

public class LabAssignmentTests extends AssignmentTests
{
    @Override
    public Assignment createAssignment ( String name , StudyForm studyForm )
    {
        return new LabAssignment( studyForm );
    }


    @Override
    public StudyForm createStudyForm ( String name )
    {
        return new StudyForm( new NonEmptyString( name ) , StudyFormType.LAB_TYPE );
    }
}
