package studentqueue.domain.unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import studentqueue.domain.AcademicGroup;
import studentqueue.domain.Student;
import studentqueue.domain.Account;
import studentqueue.utils.NonEmptyString;

import java.util.UUID;

public class GroupTests {

    private AcademicGroup ki10 = new AcademicGroup( new NonEmptyString( "ki-10-5" ) );
    private Student ivanov = new Student( new NonEmptyString( "Ivan" ) ,
                              new NonEmptyString( "Ivanov" ) ,
                              new NonEmptyString( "ii@mail.ru" ) );
    private Student petrov = new Student( new NonEmptyString( "Petr" ) ,
                              new NonEmptyString( "Petrov" ) ,
                              new NonEmptyString( "pp@ya.ru" ) );

    @Test
    public void constructorInitsCorrectly()
    {
        assertEquals( "ki-10-5" , ki10.getName() );
        assertEquals( 0 , ki10.getStudentsQuantity() );
        assertNull( ki10.getCaptain() );
    }


    @Test
    public void addStudentCorrectly()
    {
        ki10.addStudent( ivanov );

        assertEquals( ivanov , ki10.findStudent( UUID.fromString( ivanov.getUUID() ) ) );
        assertEquals( 1 , ki10.getStudentsQuantity() );
    }


    @Test
    public void changeGroupNameCorrectly()
    {
        ki10.setName( new NonEmptyString( "ki11" ) );

        assertEquals( "ki11" , ki10.getName() );
    }


    @Test( expected = Exception.class )
    public void dontAddStudentTwice()
    {
        ki10.addStudent( ivanov );
        ki10.addStudent( ivanov );
    }


    @Test
    public void addStudentTwiceWithDifferentUUID()
    {
        ki10.addStudent( ivanov );

        Student ivanov2 = new Student( new NonEmptyString( "Ivan" ) ,
                                    new NonEmptyString( "Ivanov" ) ,
                                    new NonEmptyString( "ii@mail.ru" ) );

        ki10.addStudent( ivanov2 );

        assertEquals( ivanov , ki10.findStudent( UUID.fromString( ivanov.getUUID() ) ) );
        assertEquals( ivanov2 , ki10.findStudent( UUID.fromString( ivanov2.getUUID() ) ) );
        assertEquals( 2 , ki10.getStudentsQuantity() );
    }


    @Test
    public void findStudentWhoseIsInGroup()
    {
        ki10.addStudent( ivanov );

        assertTrue( ki10.isInGroup( ivanov ) );
    }


    @Test
    public void dontFindStudentWhoseIsNotInGroup()
    {
        ki10.addStudent( ivanov );

        assertFalse( ki10.isInGroup( petrov ) );
    }


    @Test
    public void assignCaptainProperly()
    {
        ki10.addStudent( ivanov );
        ki10.assignCaptain( ivanov );

        assertEquals( ivanov.getUUID() , ki10.getCaptain().getUUID() );
    }


    @Test( expected = Exception.class )
    public void dontAssignCaptainNotInGroup()
    {
        ki10.addStudent( ivanov );

        ki10.assignCaptain( petrov );
    }


    @Test
    public void removeStudentFromGroupCorrectly ()
    {
        ki10.addStudent( ivanov );
        ki10.removeStudentFromGroup( ivanov );

        assertEquals( 0 , ki10.getStudentsQuantity() );
    }


    @Test( expected = Exception.class )
    public void removeStudentNotInGroupProvidesException ()
    {
        ki10.addStudent( ivanov );
        ki10.removeStudentFromGroup( petrov );
    }
}
