package studentqueue.domain.unittests;

import org.junit.Test;
import studentqueue.domain.Account;
import studentqueue.utils.NonEmptyString;

import static org.junit.Assert.*;
/**
 * Created by lera on 12.02.2016.
 */
public class AccountTests
{
    @Test
    public void сonstructorInitsCorrectly()
    {
        Account account = new Account( new NonEmptyString( "Ivan" ) ,
                                       new NonEmptyString( "Ivanov" ) ,
                                       new NonEmptyString( "ii@mail.ru" ) );

        assertEquals( "Ivan" , account.getFirstName() );
        assertEquals( "Ivanov" , account.getLastName() );
        assertEquals( "ii@mail.ru" , account.getEmail() );
        assertNotNull( account.getUUID() );
    }


    @Test
    public void сhangeFirstNameCorrectly()
    {
        Account account = new Account( new NonEmptyString( "Ivan" ) ,
                                       new NonEmptyString( "Ivanov" ) ,
                                       new NonEmptyString( "ii@mail.ru" ) );

        account.setFirstName( new NonEmptyString( "Vasya" ) );

        assertEquals( "Vasya" , account.getFirstName() );
    }


    @Test
    public void сhangeLastNameCorrectly()
    {
        Account account = new Account( new NonEmptyString( "Ivan" ) ,
                                       new NonEmptyString( "Ivanov" ) ,
                                       new NonEmptyString( "ii@mail.ru" ) );

        account.setLastName( new NonEmptyString( "Pupkin" ) );

        assertEquals( "Pupkin" , account.getLastName() );
    }


    @Test
    public void сhangeEmailCorrectly()
    {
        Account account = new Account( new NonEmptyString( "Ivan" ) ,
                                       new NonEmptyString( "Ivanov" ) ,
                                       new NonEmptyString( "ii@mail.ru" ) );

        account.setEmail( new NonEmptyString( "gg@ya.ru" ) );

        assertEquals( "gg@ya.ru" , account.getEmail() );
    }
}
