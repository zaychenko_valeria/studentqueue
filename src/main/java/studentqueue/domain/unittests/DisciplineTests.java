package studentqueue.domain.unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import studentqueue.domain.*;

import studentqueue.utils.NonEmptyString;

public class DisciplineTests
{
/**
 * Constructor_Inits_Correctly()
 * Set_Description_Correctly()
 * Add_Lab_Assignments_Properly()
 * Count_Lab_Assignments_Quantity_Properly()
 * Add_Test_Points_Properly()
 * Count_Test_Assignments_Quantity_Properly()
 * Find_LabStudyForm_Correctly()
 * Find_LabStudyForm_Dont_Exist()
 * Find_LectureStudyForm_Correctly()
 * Find_LectureStudyForm_Dont_Exist()
 */

    @Test
    public void constructorInitsCorrectly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );

        assertEquals( ipz.getName() , "ipz" );
    }


    @Test
    public void setDescriptionCorrectly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        ipz.setDescription( new NonEmptyString( "The goal of ipz is..." ) );

        assertEquals( ipz.getDescription() , "The goal of ipz is..." );
    }


    @Test
    public void addLabAssignmentsProperly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lb1 = new StudyForm( new NonEmptyString( "lb1IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lb1 );
        LabAssignment labAssignment = new LabAssignment( lb1 );
        labAssignment.addLevelToAssignment( LevelEnum.ADVANCED, new AssignmentRange( 20 , 30 ) );
        ipz.addAssignment( labAssignment );

        StudyForm ipzStudyForm = ipz.getStudyForms( 0 );
        Assignment labAss = ipz.getAssignment( 0 );

        assertEquals( ipzStudyForm.getName() , "lb1IPZ" );

        assertEquals( labAss.getRangeOfAllLevels().getLow() , 20 );
        assertEquals( labAss.getRangeOfAllLevels().getHigh() , 30 );
    }


    @Test
    public void countLabAssignmentsQuantityProperly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lab1 = new StudyForm( new NonEmptyString( "lab1IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab1 );
        LabAssignment lab1Assignment = new LabAssignment( lab1 );
        lab1Assignment.addLevelToAssignment( LevelEnum.ADVANCED, new AssignmentRange( 20 , 30 ) );
        ipz.addAssignment( lab1Assignment );

        StudyForm lab2 = new StudyForm( new NonEmptyString( "lab2IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab2 );
        LabAssignment lab2Assignment = new LabAssignment( lab2 );
        lab2Assignment.addLevelToAssignment( LevelEnum.BEGINNER, new AssignmentRange( 10 , 20 ) );
        ipz.addAssignment( lab2Assignment );

        assertEquals( ipz.getLabAssignmentsCount() , 2 );
    }


    @Test
    public void findLabStudyFormCorrectly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lab1 = new StudyForm( new NonEmptyString( "lab1IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab1 );
        StudyForm lab2 = new StudyForm( new NonEmptyString( "lab2IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab2 );

        StudyForm finder = ipz.findStudyForm( lab2.getName() , StudyFormType.LAB_TYPE );

        assertEquals( finder.getName() , lab2.getName() );
    }


    @Test
    public void findLabStudyFormDontExist()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lab1 = new StudyForm( new NonEmptyString( "lab1IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab1 );
        StudyForm lab2 = new StudyForm( new NonEmptyString( "lab2IPZ" ) , StudyFormType.LAB_TYPE );
        ipz.addStudyForm( lab2 );

        StudyForm finder = ipz.findStudyForm( "lab3IPZ" , StudyFormType.LAB_TYPE );
        assertNull( finder );
    }


    @Test
    public void findLectureStudyFormCorrectly()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lc1 = new StudyForm( new NonEmptyString( "lc1IPZ" ) , StudyFormType.LECTURE_TYPE );
        ipz.addStudyForm( lc1 );
        StudyForm lc2 = new StudyForm( new NonEmptyString( "lc2IPZ" ) , StudyFormType.LECTURE_TYPE );
        ipz.addStudyForm( lc2 );

        StudyForm finder = ipz.findStudyForm( lc2.getName() , StudyFormType.LECTURE_TYPE );

        assertEquals( finder.getName() , lc2.getName() );
    }


    @Test
    public void findLectureStudyFormThatDoesntExist()
    {
        Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
        StudyForm lc1 = new StudyForm( new NonEmptyString( "lc1IPZ" ) , StudyFormType.LECTURE_TYPE );
        ipz.addStudyForm( lc1 );
        StudyForm lc2 = new StudyForm( new NonEmptyString( "lc2IPZ" ) , StudyFormType.LECTURE_TYPE );
        ipz.addStudyForm( lc2 );

        StudyForm finder = ipz.findStudyForm( "lc3IPZ" , StudyFormType.LECTURE_TYPE );
        assertNull( finder );
    }
}
