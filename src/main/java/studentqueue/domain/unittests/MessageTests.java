package studentqueue.domain.unittests;

import org.junit.Test;
import static org.junit.Assert.*;

import studentqueue.domain.Attachment;
import studentqueue.domain.Message;
import studentqueue.domain.Student;
import studentqueue.utils.NonEmptyString;

/**
 * Created by lera on 06.03.2016.
 */
public class MessageTests
{
    Student student = new Student( new NonEmptyString( "Ivan" ) ,
                                new NonEmptyString( "Ivanov" ) ,
                                 new NonEmptyString( "ii@mail.ru" ) );


    @Test
    public void constructorInitsCorrectly()
    {
        String text = "This is first message";
        Message message = new Message( text , student );

        assertEquals( message.getText() , text );
        assertFalse( message.isThereAttachment() );
    }


    @Test
    public void addAttachmentCorrectly()
    {
        Message message = new Message( "This is message with attachment" , student );
        message.addAttachment( new Attachment( null , "Lab.txt" ) );

        assertEquals( message.isThereAttachment() , true );
    }
}
