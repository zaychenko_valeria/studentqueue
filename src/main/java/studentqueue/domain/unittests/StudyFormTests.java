package studentqueue.domain.unittests;

import static org.junit.Assert.*;
import org.junit.Test;

import studentqueue.domain.StudyForm;
import studentqueue.domain.StudyFormType;
import studentqueue.utils.NonEmptyString;


public class StudyFormTests
{
    @Test
    public void constuctorLabInitsPropertiesCorrectly()
    {
        StudyForm lab1 = new StudyForm( new NonEmptyString( "Lab1" ) , StudyFormType.LAB_TYPE );

        assertEquals( lab1.getName() , "Lab1" );
        assertEquals( lab1.getDescription() , "" );
        assertEquals( lab1.getStudyFormType() , StudyFormType.LAB_TYPE );
    }


    @Test
    public void constuctorLectureInitPropertiesCorrectly()
    {
        StudyForm lecture1 = new StudyForm( new NonEmptyString( "Lecture1" ) , StudyFormType.LECTURE_TYPE );

        assertEquals( lecture1.getName() , "Lecture1" );
        assertEquals( lecture1.getDescription() , "" );
        assertEquals( lecture1.getStudyFormType() , StudyFormType.LECTURE_TYPE );
    }


    @Test( expected = Exception.class )
    public void constructorDoesntInitsEmptyName()
    {
        new StudyForm( new NonEmptyString( "" ) , StudyFormType.LAB_TYPE );
    }


    @Test( expected = Exception.class )
    public void constructorDoesntInitIncorrectStudyFormType()
    {
        new StudyForm( new NonEmptyString( "" ) , StudyFormType.valueOf( "Incorrect" ) );
    }


    @Test
    public void setDescriptionProperly()
    {
        StudyForm lab1 = new StudyForm( new NonEmptyString( "lab1" ) , StudyFormType.LAB_TYPE );
        lab1.setDescription( new NonEmptyString( "This is a lab1" ) );

        assertEquals( lab1.getDescription() , "This is a lab1" );
    }

}
