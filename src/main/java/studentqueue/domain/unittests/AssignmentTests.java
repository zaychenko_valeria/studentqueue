package studentqueue.domain.unittests;

import static org.junit.Assert.*;
import org.junit.Test;

import java.time.LocalDate;

import studentqueue.domain.Assignment;
import studentqueue.domain.LevelEnum;
import studentqueue.domain.AssignmentRange;
import studentqueue.domain.StudyForm;

/**
 * Created by lera on 12.02.2016.
 */
public abstract class AssignmentTests
{
    private StudyForm studyForm = createStudyForm( "Test1Lab" );
    private Assignment assignment1 = createAssignment( "Test1Lab" , studyForm );
    private AssignmentRange beginner = new AssignmentRange( 10 , 30 );

    public abstract Assignment createAssignment( String name , StudyForm studyForm );
    public abstract StudyForm createStudyForm( String name );


    @Test
    public void constructorInitsProperly()
    {
        assertEquals( assignment1.getName() , "Test1Lab" );
    }


    @Test
    public void addLevelToAssignmentCorrectly()
    {
        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner );

        assertEquals( assignment1.getRangeOfLevel( LevelEnum.BEGINNER ) ,  beginner );
    }


    @Test
    public void getRangeAllLevelsCorrectWithOneLevel()
    {
        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner );

        assertEquals( assignment1.getRangeOfAllLevels().getLow() , 10 );
        assertEquals( assignment1.getRangeOfAllLevels().getHigh() , 30 );
    }


    @Test
    public void getRangeAllLevelsCorrectWithTwoLevels()
    {
        AssignmentRange intermediate = new AssignmentRange( 20 , 50 );
        assignment1.addLevelToAssignment( LevelEnum.INTERMEDIATE, intermediate );

        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner );

        assertEquals( assignment1.getRangeOfAllLevels().getLow() , 30 );
        assertEquals( assignment1.getRangeOfAllLevels().getHigh() , 80 );
    }


    @Test( expected = Exception.class )
    public void DontAddLevelOverMaxRange()
    {
        AssignmentRange intermediate = new AssignmentRange( 20 , 80 );
        assignment1.addLevelToAssignment( LevelEnum.INTERMEDIATE, intermediate );

        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner );
    }


    @Test
    public void getRangeAllLevelsHighEqualsMaxPoint()
    {
        AssignmentRange intermediate = new AssignmentRange( 20 , 70 );
        assignment1.addLevelToAssignment( LevelEnum.INTERMEDIATE, intermediate );

        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner );

        assertEquals( assignment1.getRangeOfAllLevels().getLow() , 30 );
        assertEquals( assignment1.getRangeOfAllLevels().getHigh() , 100 );
    }


    @Test
    public void changeLevelValueCorrectly()
    {
        AssignmentRange beginner2 = new AssignmentRange( 20 , 70 );

        assignment1.addLevelToAssignment( LevelEnum.BEGINNER, beginner2 );

        assertEquals( assignment1.getRangeOfAllLevels().getLow() , 20 );
        assertEquals( assignment1.getRangeOfAllLevels().getHigh() , 70 );
    }


    @Test
    public void setDeadlineCorrectly()
    {
        LocalDate deadline1 = LocalDate.of( 2016 , 10, 14 );
        assignment1.setDeadline( deadline1 );

        assertEquals( assignment1.getDeadlineDate() , deadline1 );
    }


    @Test( expected = Exception.class )
    public void dontSetDeadlineBeforeNow()
    {
        LocalDate deadline1 = LocalDate.of( 2014 , 10, 14 );
        assignment1.setDeadline( deadline1 );
    }


    @Test
    public void deadlineNotMissedNow()
    {
        LocalDate deadline1 =LocalDate.of( 2016 , 11, 14 );
        assignment1.setDeadline( deadline1 );

        assertFalse( assignment1.deadlineMissed( LocalDate.now() ) );
    }


    @Test
    public void deadlineMissedCorrectly()
    {
        LocalDate deadline1 =LocalDate.of( 2016 , 11, 14 );
        assignment1.setDeadline( deadline1 );

        assertTrue( assignment1.deadlineMissed( LocalDate.of( 2016 , 12 , 14 ) ) );
    }
}
