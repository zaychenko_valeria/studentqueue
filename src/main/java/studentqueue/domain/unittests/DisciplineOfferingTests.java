package studentqueue.domain.unittests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import studentqueue.domain.*;
import studentqueue.utils.NonEmptyString;

public class DisciplineOfferingTests
{
/**
 * Constructor_Inits_Correctly()
 * Change_Form_Control_Correctly()
 * Assign_Group_ToDiscipline_Correctly()
 * Dont_Assign_Group_Already_Listened_Discipline()
 */

    private LocalDate startDateIPZ = LocalDate.of( 2015 , 9 , 1 );
    private LocalDate finishDateIPZ = LocalDate.of( 2016 , 1 , 10 );
    private Discipline ipz = new Discipline( new NonEmptyString( "ipz" ) );
    private Instructor zaychenko = new Instructor( new NonEmptyString( "Sergei" ) ,
                                                   new NonEmptyString( "Zaychenko" ) ,
                                                   new NonEmptyString( "sz@gmail.com" ) );


    @Test
    public void constructorInitsCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        assertEquals( disciplineOffering2015IPZ.getStartDate() , startDateIPZ );
        assertEquals( disciplineOffering2015IPZ.getFinishDate() , finishDateIPZ );
        assertEquals( disciplineOffering2015IPZ.getDiscipline() , ipz );
        assertEquals( disciplineOffering2015IPZ.getFormOfControl() , FormOfControl.WRITTEN_EXAMINATION );
        assertEquals( 0 , disciplineOffering2015IPZ.getQuantityOfGroups() );
    }


    @Test
    public void changeFormControlCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                                ipz , FormOfControl.CREDIT , zaychenko );
        disciplineOffering2015IPZ.setFormOfControl( FormOfControl.WRITTEN_EXAMINATION );

        assertEquals( FormOfControl.WRITTEN_EXAMINATION, disciplineOffering2015IPZ.getFormOfControl() );
    }


    @Test
    public void assignGroupToDisciplineCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );
        AcademicGroup ki10 = new AcademicGroup( new NonEmptyString( "ki-10-1" ) );
        disciplineOffering2015IPZ.assignGroupToOffering( ki10 );

        assertEquals( 1 , disciplineOffering2015IPZ.getQuantityOfGroups() );
        assertEquals( true , disciplineOffering2015IPZ.hasGroup( ki10 ) );
    }


    @Test( expected = Exception.class )
    public void dontAssignGroupAlreadyListenedDiscipline()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );
        AcademicGroup ki10 = new AcademicGroup( new NonEmptyString( "ki-10-1" ) );
        disciplineOffering2015IPZ.assignGroupToOffering( ki10 );
        disciplineOffering2015IPZ.assignGroupToOffering( ki10 );
    }


    @Test
    public void addAssistantCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        Instructor assistant = new Instructor( new NonEmptyString( "an" ) ,
                                               new NonEmptyString( "ziarmand" ) ,
                                               new NonEmptyString( "az@f" ) );
        disciplineOffering2015IPZ.addAssistantToOffering( assistant );

        assertEquals( disciplineOffering2015IPZ.getQuantityOfAssistants() , 1 );
        assertTrue( disciplineOffering2015IPZ.isAssistant( assistant ) );
    }


    @Test( expected = Exception.class )
    public void dontAddAssistantAlreadyOfferingHas()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        Instructor assistant = new Instructor( new NonEmptyString( "an" ) ,
                                               new NonEmptyString( "ziarmand" ) ,
                                               new NonEmptyString( "az@f" ) );
        disciplineOffering2015IPZ.addAssistantToOffering( assistant );
        disciplineOffering2015IPZ.addAssistantToOffering( assistant );
    }


    @Test( expected = Exception.class )
    public void dontAddLecturerAsSelfAssistant()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        disciplineOffering2015IPZ.addAssistantToOffering( zaychenko );
    }


    @Test
    public void addNewLecturerCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        Instructor lecturer = new Instructor( new NonEmptyString( "an" ) ,
                                              new NonEmptyString( "ziarmand" ) ,
                                              new NonEmptyString( "az@f" ) );

        disciplineOffering2015IPZ.addLecturerToOffering( lecturer );

        assertEquals( disciplineOffering2015IPZ.getLecturer().getUUID() , lecturer.getUUID() );
    }


    @Test
    public void addTheSameLecturerCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        disciplineOffering2015IPZ.addLecturerToOffering( zaychenko );

        assertEquals( disciplineOffering2015IPZ.getLecturer().getUUID() , zaychenko.getUUID() );
    }


    @Test
    public void addAssistantAsLecturerCorrectly()
    {
        DisciplineOffering disciplineOffering2015IPZ = new DisciplineOffering( startDateIPZ , finishDateIPZ ,
                ipz , FormOfControl.WRITTEN_EXAMINATION , zaychenko );

        Instructor assistant = new Instructor( new NonEmptyString( "an" ) ,
                                               new NonEmptyString( "ziarmand" ) ,
                                               new NonEmptyString( "az@f" ) );

        disciplineOffering2015IPZ.addAssistantToOffering( assistant );
        disciplineOffering2015IPZ.addLecturerToOffering( assistant );

        assertEquals( disciplineOffering2015IPZ.getLecturer().getUUID() , assistant.getUUID() );
        assertFalse( disciplineOffering2015IPZ.isAssistant( assistant ) );
    }
}
