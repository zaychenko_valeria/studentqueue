package studentqueue.domain.unittests;

import static org.junit.Assert.*;


import org.junit.Test;

import studentqueue.domain.*;
import studentqueue.utils.NonEmptyString;


public class TestAssignmentTests extends AssignmentTests{


    @Override
    public Assignment createAssignment ( String name , StudyForm studyForm )
    {
        return new TestAssignment( new NonEmptyString( name ) );
    }

    @Override
    public StudyForm createStudyForm ( String name )
    {
        return null;
    }
}
