package studentqueue.domain.unittests;

import static org.junit.Assert.*;

import org.junit.Test;

import studentqueue.domain.AssignmentRange;


public class AssignmentRangeTests
{
    @Test
    public void constructorInitsCorrectly()
    {
        AssignmentRange range = new AssignmentRange( 10 , 20 );
        assertEquals( range.getLow() , 10 );
        assertEquals( range.getHigh() , 20 );
    }


    @Test( expected = Exception.class )
    public void constuctorDoesntInitNegativeLow()
    {
        new AssignmentRange( -10 , 20 );
    }


    @Test( expected = Exception.class )
    public void constuctorDoesntInitNegativeHigh()
    {
        new AssignmentRange( 10 , -20 );
    }


    @Test( expected = Exception.class )
    public void constuctorDoesntInitLowOverHigh()
    {
        new AssignmentRange( 10 , 5 );
    }


    @Test( expected = Exception.class )
    public void constuctorDoesntInitHighOver100()
    {
        new AssignmentRange( 10 , 200 );
    }


    @Test
    public void checkValueUnderLowRangeContains()
    {
        AssignmentRange range = new AssignmentRange( 10 , 20 );
        assertFalse( range.isInAssignmentRange( 0 ) );
    }


    @Test
    public void checkValueOverHighRangeContains()
    {
        AssignmentRange range = new AssignmentRange( 10 , 20 );
        assertFalse( range.isInAssignmentRange( 30 ) );
    }


    @Test
    public void checkValueEqualsLowRangeContains()
    {
        AssignmentRange range = new AssignmentRange( 10 , 20 );
        assertTrue( range.isInAssignmentRange( 10 ) );
    }


    @Test
    public void checkValueEqualsHighRangeContains()
    {
        AssignmentRange range = new AssignmentRange( 10 , 20 );
        assertTrue( range.isInAssignmentRange( 20 ) );
    }


    @Test
    public void sumRangesCorrectly()
    {
        AssignmentRange range1 = new AssignmentRange( 10 , 20 );
        AssignmentRange range2 = new AssignmentRange( 20 , 60 );

        AssignmentRange newRange = range1.sumRanges( range2 );

        assertEquals( newRange.getLow() , 30 );
        assertEquals( newRange.getHigh() , 80 );
    }
}
