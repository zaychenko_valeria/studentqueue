package studentqueue.domain;

import studentqueue.utils.EnumUtils;

public enum FormOfControl
{
    WRITTEN_EXAMINATION, COMBINED_EXAMINATION, CREDIT;

    public static FormOfControl fromString( String name )
    {
        return EnumUtils.getEnumFromString( FormOfControl.class , name );
    }
}
