package studentqueue.domain;

import studentqueue.utils.NonEmptyString;

import javax.persistence.*;
import java.util.UUID;

@Entity
@PrimaryKeyJoinColumn
public class Instructor extends Account
{
    public Instructor( NonEmptyString firstName , NonEmptyString lastName , NonEmptyString email )
    {
        super( firstName , lastName , email );
    }

    protected Instructor(){}
}
