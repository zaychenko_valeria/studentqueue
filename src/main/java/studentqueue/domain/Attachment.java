package studentqueue.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;

/**
 * Created by lera on 06.03.2016.
 */
@Entity
public class Attachment
{
    public Attachment( byte[] byteArray , String filename )
    {
        this.data = data;
        this.filename = filename;
    }

    protected Attachment(){}

    @Id
    @GeneratedValue
    private long id;

    private byte[] data;
    private String filename;
}
