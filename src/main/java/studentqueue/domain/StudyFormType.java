package studentqueue.domain;

import static studentqueue.utils.EnumUtils.getEnumFromString;


/**
 * Created by lera on 10.02.2016.
 */

public enum StudyFormType
{
    LECTURE_TYPE , LAB_TYPE ;

    public static StudyFormType fromString( String name )
    {
        return getEnumFromString( StudyFormType.class , name );
    }
}
