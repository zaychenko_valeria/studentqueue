package studentqueue.domain;


import studentqueue.utils.NonEmptyString;
import studentqueue.viewmodel.FullStudentView;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn
public class Student extends Account
{
    public Student( NonEmptyString firstName , NonEmptyString lastName , NonEmptyString email )
    {
        super( firstName , lastName , email );
    }

    protected Student(){}
}
