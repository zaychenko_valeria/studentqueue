package studentqueue.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by lera on 06.03.2016.
 */

@Entity
public class Message
{
    public Message( String text , Account sender )
    {
        this.text = text;
        this.sender = sender;
        postingDate = System.currentTimeMillis();
    }

    protected Message(){}

    public void addAttachment( Attachment attachment )
    {
        this.attachment = attachment;
    }


    public String getText()
    {
        return text;
    }

    public long getPostingDate ()
    {
        return postingDate;
    }

    public Account getSender ()
    {
        return sender;
    }

    public boolean isThereAttachment()
    {
        return attachment != null;
    }


    @Id
    @GeneratedValue
    private long id;

    private long postingDate;
    private String text;

    @OneToOne
    private Account sender;

    @OneToOne
    private Attachment attachment;
}
