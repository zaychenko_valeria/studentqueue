package studentqueue.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by lera on 06.03.2016.
 */
@Entity
public class Discussion
{
    public Discussion( String subject , Student owner , Instructor recipient , Message message )
    {
        this.subject = subject;
        this.owner = owner;
        this.recipient = recipient;
        this.uuid = UUID.randomUUID().toString();

        messages = new ArrayList< Message >();
        messages.add( message );
    }


    protected Discussion(){}


    public String getSubject()
    {
        return subject;
    }


    public Student getOwner()
    {
        return owner;
    }


    public int getMessagesCount()
    {
        return messages.size();
    }


    public long getId()
    {
        return id;
    }


    public String getUuid()
    {
        return uuid;
    }


    public Instructor getRecipient()
    {
        return recipient;
    }


    public Iterator< Message > getMessagesIterator()
    {
        return messages.iterator();
    }


    public void addMessage( Message message )
    {
        messages.add( message );
    }


    @Id
    @GeneratedValue
    private long id;

    private String subject;
    private String uuid;

    @OneToOne
    private Student owner;

    @OneToOne
    private Instructor recipient;

    @OneToMany( cascade=CascadeType.ALL )
    private List< Message > messages;
}
