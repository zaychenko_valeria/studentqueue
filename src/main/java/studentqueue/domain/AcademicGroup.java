package studentqueue.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import studentqueue.utils.NonEmptyString;

import javax.persistence.*;

@Entity
public class AcademicGroup
{
//*******    CONSTRUCTOR    **********************************************************

    public AcademicGroup ( NonEmptyString name )
    {
        this.name = name.getValue();
        this.students = new ArrayList< Student >();
    }


    protected AcademicGroup (){}

//*******    GETTERS    **************************************************************

    public String getName()
    {
        return name;
    }


    public Student getCaptain()
    {
        return captain;
    }


    public int getStudentsQuantity()
    {
        return students.size();
    }


    public Student findStudent( UUID uuid )
    {
        return students.stream()
                .filter( student -> student.getUUID().equals( uuid.toString() ) )
                .findFirst().orElse( null );
    }


    public Iterator< Student > getIterator()
    {
        return students.iterator();
    }

 // *******    SETTERS    *************************************************************

    public void setName ( NonEmptyString name )
    {
        this.name = name.getValue();
    }

//*******    PUBLIC METHODS    *******************************************************

    public void addStudent( Student student )
    {
        if ( isInGroup( student ) )
            throw new RuntimeException( "This group already has this student " + student.getFirstName() + student.getLastName() );
        this.students.add( student );
    }


    public void assignCaptain( Student captain )
    {
        if ( ! isInGroup( captain ) )
            throw new RuntimeException( "Captain must be student of group which assigned" );
        this.captain = captain;
    }


    public boolean isInGroup( Student student )
    {
        return students.stream()
                .anyMatch( s -> s.getUUID().equals( student.getUUID() ) );
    }


    public void removeStudentFromGroup ( Student student )
    {
        if ( ! isInGroup( student ) )
            throw new RuntimeException ( "This student isn't in this group" );
        students.remove( student );
    }

//*******    ATTRIBUTES    ***********************************************************

    @Id
    @GeneratedValue
    private long id;

    //@Basic
    private String name;

    @OneToOne
    private Student captain;

    @OneToMany( cascade = CascadeType.PERSIST )
    private List< Student > students;
}
