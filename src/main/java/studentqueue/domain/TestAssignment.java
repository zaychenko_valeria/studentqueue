package studentqueue.domain;

import studentqueue.utils.NonEmptyString;

import javax.persistence.Entity;

@Entity
public class TestAssignment extends Assignment
{
//*******    CONSTRUCTORS    *********************************************************


    public TestAssignment( NonEmptyString name )
    {
        this.name = name.getValue();
    }


    protected TestAssignment(){ }


//*******    GETTERS    **************************************************************


    public String getName()
    {
        return name;
    }


//*******    PUBLIC METHODS    *******************************************************


    @Override
    public AssignmentType getType ()
    {
        return AssignmentType.TEST;
    }


//*******    ATTRIBUTES    ***********************************************************


    private String name;

}
