package studentqueue.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
public class DisciplineOffering
{


//*******    CONSTRUCTORS    *********************************************************


    public DisciplineOffering ( LocalDate startDate , LocalDate finishDate ,
                                Discipline discipline , FormOfControl formOfControl , Instructor lecturer )
    {
        if( finishDate.isBefore( startDate ) )
            throw new RuntimeException( "Finish date can't be earlier then start date" );

        this.startDate = startDate;
        this.finishDate = finishDate;
        this.discipline = discipline;
        this.lecturer = lecturer;

        this.groupsOfDiscipline = new HashSet< AcademicGroup >();
        this.assistants = new ArrayList< Instructor >();
        this.formOfControl = formOfControl;
        this.uuid = UUID.randomUUID().toString();
    }


    protected DisciplineOffering (){ }


//*******    GETTERS    **************************************************************


    public String getUUID()
    {
        return this.uuid;
    }


    public LocalDate getStartDate()
    {
        return startDate;
    }


    public LocalDate getFinishDate()
    {
        return finishDate;
    }


    public FormOfControl getFormOfControl()
    {
        return formOfControl;
    }


    public Discipline getDiscipline()
    {
        return discipline;
    }


    public int getQuantityOfGroups() { return groupsOfDiscipline.size(); }


    public int getQuantityOfAssistants()
    {
        return assistants.size();
    }


    public Instructor getLecturer ()
    {
        return lecturer;
    }


    public boolean isAssistant( Instructor assistant )
    {
        return assistants.contains( assistant );
    }


//*******    PUBLIC METHODS    *******************************************************


    public boolean hasGroup( AcademicGroup academicGroup )
    {
        return groupsOfDiscipline.contains( academicGroup );
    }


    public void setDate( LocalDate startDate , LocalDate finishDate )
    {
        this.startDate = startDate;
        this.finishDate = finishDate;
    }


    public void assignGroupToOffering( AcademicGroup academicGroup )
    {
        if( groupsOfDiscipline.contains( academicGroup ) )
            throw new RuntimeException( "This group " + academicGroup.getName() + " already listen offering" );
        groupsOfDiscipline.add( academicGroup );
    }


    public void addAssistantToOffering( Instructor instructor )
    {
        if( assistants.contains( instructor ) )
            throw new RuntimeException( "This instructor already participates in the offering" );
        if( instructor.getUUID() == lecturer.getUUID() )
            throw new RuntimeException( "Lecturer can't be self-assistant" );
        assistants.add( instructor );
    }


    public void addLecturerToOffering( Instructor instructor )
    {
        if( assistants.contains( instructor ) )
            assistants.remove( instructor );
        lecturer = instructor;
    }


    public void setFormOfControl( FormOfControl formOfControl )
    {
        this.formOfControl = formOfControl;
    }


    public void removeGroupFromOffering ( AcademicGroup academicGroup )
    {
        if ( ! hasGroup( academicGroup ) )
            throw new RuntimeException( "This group isn't listen this course this year" );
        groupsOfDiscipline.remove( academicGroup );
    }


    public boolean isListeningOffering( AcademicGroup academicGroup )
    {
        return groupsOfDiscipline.contains( academicGroup );
    }


//*******    ATTRIBURES    ***********************************************************


    @Id
    @GeneratedValue
    private long id;

    private LocalDate startDate;
    private LocalDate finishDate;
    private String uuid;

    @OneToOne
    private Discipline discipline;

    @OneToMany
    private Set< AcademicGroup > groupsOfDiscipline;

    private FormOfControl formOfControl;

    @OneToMany
    private List< Instructor > assistants;

    @OneToOne
    private Instructor lecturer;
}
