package studentqueue.domain;

import studentqueue.utils.EnumUtils;

public enum LevelEnum
{
    BEGINNER, INTERMEDIATE, ADVANCED;


    public static LevelEnum fromString( String name )
    {
        return EnumUtils.getEnumFromString( LevelEnum.class , name );
    }
}
