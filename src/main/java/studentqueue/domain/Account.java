package studentqueue.domain;


import studentqueue.utils.NonEmptyString;
import studentqueue.viewmodel.AccountView;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Inheritance( strategy=InheritanceType.JOINED )
public class Account implements AccountView
{
//*******    CONSTRUCTOR    **********************************************************

    public Account( NonEmptyString firstName , NonEmptyString lastName , NonEmptyString email )
    {
        this.firstName = firstName.getValue();
        this.lastName = lastName.getValue();
        this.email = email.getValue();
        uuid = UUID.randomUUID().toString();
    }


    public Account(){}

//*******    GETTERS    **************************************************************

    @Override
    public String getFirstName()
    {
        return firstName;
    }


    @Override
    public String getLastName()
    {
        return lastName;
    }


    @Override
    public String getEmail()
    {
        return email;
    }


    @Override
    public String getUUID ()
    {
        return uuid;
    }

// *******    SETTERS    *************************************************************

    public void setFirstName( NonEmptyString firstName )
    {
        this.firstName = firstName.getValue();
    }


    public void setLastName( NonEmptyString lastName )
    {
        this.lastName = lastName.getValue();
    }


    public void setEmail ( NonEmptyString email )
    {
        this.email = email.getValue();
    }

//*******    ATTRIBUTES    ***********************************************************

    @Id
    @GeneratedValue
    private long id;

    private String uuid;
    private String firstName;
    private String lastName;
    private String email;
}
