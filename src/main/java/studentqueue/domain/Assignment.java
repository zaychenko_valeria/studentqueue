package studentqueue.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.EnumMap;
import java.util.Map;


/**
 * Assignment represent date of deadline
 * and range of score for levels in LevelEnum
 */

@Entity
@Inheritance( strategy = InheritanceType.TABLE_PER_CLASS )
public abstract class Assignment
{
//******    CONSTRUCTOR    ********************************************************************


    protected Assignment()
    {
        this.rangeOfScoreForEveryLevel =
                new EnumMap< LevelEnum , AssignmentRange >( LevelEnum.class );
    }


//******    GETTERS    ***********************************************************************


    public AssignmentRange getRangeOfLevel( LevelEnum level )
    {
        return rangeOfScoreForEveryLevel.get( level );
    }


    public LocalDate getDeadlineDate()
    {
        return deadline;
    }


//******    SETTERS    ***********************************************************************


    public void setDeadline( LocalDate date )
    {
        if ( date.isBefore( LocalDate.now() ) )
            throw new RuntimeException( "Deadline must be setted in the future" );
        this.deadline = date;
    }


//******    ABSTRACT METHODS    **************************************************************


    public abstract String getName();

    public abstract AssignmentType getType();


//******    PUBLIC METHODS    ****************************************************************

/**
 * @return summary range of all levels in assignment in range ( 0 , 100 )
 */
    public AssignmentRange getRangeOfAllLevels()
    {
        int min = 0;
        int max = 0;

        for ( AssignmentRange levelRange : rangeOfScoreForEveryLevel.values() )
        {
            min += levelRange.getLow();
            max += levelRange.getHigh();
        }

        return new AssignmentRange( min , max );
    }


    public void addLevelToAssignment( LevelEnum level , AssignmentRange range )
    {
        if ( getRangeOfAllLevels().getHigh() +  range.getHigh() > 100 )
            throw new RuntimeException( "Your lab is going over 100 points " );

        this.rangeOfScoreForEveryLevel.put( level , range );
    }


    public void removeLevelFromAssignment( LevelEnum levelEnum )
    {
        if( ! rangeOfScoreForEveryLevel.containsKey( levelEnum ) )
            throw new RuntimeException( "There is no such level" );

        rangeOfScoreForEveryLevel.remove( levelEnum );
    }


    public boolean deadlineMissed( LocalDate date)
    {
        return date.isAfter( deadline );
    }


//******    ATTRIBUTES    *********************************************************************

    @Id
    private long id;

    @ElementCollection( fetch = FetchType.LAZY )
    private Map< LevelEnum , AssignmentRange > rangeOfScoreForEveryLevel;

    private LocalDate deadline;

}
