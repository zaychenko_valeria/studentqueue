package studentqueue.domain;

import studentqueue.utils.EnumUtils;

/**
 * Created by lera on 11.02.2016.
 */
public enum AssignmentType
{
    LAB , TEST;

    public static AssignmentType fromString( String name )
    {
        return EnumUtils.getEnumFromString( AssignmentType.class , name );
    }
}
