package studentqueue.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class LabAssignment extends Assignment
{
//*******    CONSTRUCTOR    *******************************************************


    public LabAssignment( StudyForm studyForm )
    {
        if( studyForm.getStudyFormType() != StudyFormType.LAB_TYPE )
            throw new RuntimeException( "You can't create lab from lecture" );
        this.studyForm = studyForm;
    }


    protected LabAssignment(){ }


//*******    GETTERS    ***********************************************************


    @Override
    public String getName ()
    {
        return studyForm.getName();
    }


    public StudyForm getStudyForm()
    {
        return studyForm;
    }


//*******    PUBLIC METHODS    ****************************************************


    @Override
    public AssignmentType getType ()
    {
        return AssignmentType.LAB;
    }


//*******    ATTRIBUTES    *******************************************************

    @OneToOne
    private StudyForm studyForm;

}
