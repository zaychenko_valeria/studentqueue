package studentqueue.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Created by lera on 20.02.2016.
 */

@Entity
public class AssignmentOffering
{
    public AssignmentOffering( Assignment assignment , DisciplineOffering disciplineOffering )
    {
        this.assignment = assignment;
        this.disciplineOffering = disciplineOffering;
        this.startDate = disciplineOffering.getStartDate();
        this.finishDate = disciplineOffering.getFinishDate();
        this.uuid = UUID.randomUUID().toString();
    }

    protected AssignmentOffering(){ }


    public void setStartDate( LocalDate startDate )//TODO what if discipline offering date changes after setting startDate assignment offering
    {
        if( startDate.isAfter( disciplineOffering.getStartDate() ) &&
                startDate.isBefore( disciplineOffering.getFinishDate() ) )
            if ( startDate.isBefore( finishDate ) )
            {
                this.startDate = startDate;
                return;
            }
        throw new RuntimeException( "StartDate isn't valid" );
    }


    public void setFinishDate( LocalDate finishDate )//TODO the same question
    {
        if( finishDate.isAfter( disciplineOffering.getStartDate() ) &&
                finishDate.isBefore( disciplineOffering.getFinishDate() ) )
            if ( finishDate.isAfter( startDate ) )
            {
                this.finishDate = finishDate;
                return;
            }
        throw new RuntimeException( "FinishDate isn't valid" );
    }


    public String getUuid ()
    {
        return uuid;
    }


    public LocalDate getStartDate ()
    {
        return startDate;
    }


    public LocalDate getFinishDate ()
    {
        return finishDate;
    }


    @Id
    @GeneratedValue
    private long id;

    private String uuid;
    private LocalDate startDate;
    private LocalDate finishDate;

    @OneToOne
    private Assignment assignment;

    @ManyToOne
    private DisciplineOffering disciplineOffering;
}
