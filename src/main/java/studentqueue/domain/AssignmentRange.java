package studentqueue.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
* AssignmentRange represent a low and high bound of the grade
*/
@Embeddable
public class AssignmentRange
{
//******    CONSTRUCTOR    ************************************************************


    public AssignmentRange( int low , int high )
    {
        if ( low < 0 )
            throw new RuntimeException( "Minimal value of rating`s range is negative" );
        if ( low > high )
            throw new RuntimeException( "Minimal value of rating`s range is over maximum" );
        if ( high > 100 )
            throw new RuntimeException( "Maximum value is over possible 100p score" );

        this.low = low;
        this.high = high;
    }


    protected AssignmentRange(){}


//******    GETTERS    ****************************************************************


    public int getLow()
    {
        return low;
    }


    public int getHigh()
    {
        return high;
    }


//******    PUBLIC METHODS    *********************************************************


    public boolean isInAssignmentRange( int number )
    {
        return ( ( number >= low ) && ( number <= high ) );
    }


    public AssignmentRange sumRanges( AssignmentRange otherRange )
    {
        this.low += otherRange.getLow();
        this.high += otherRange.getHigh();

        return new AssignmentRange( low , high );
    }


//*************     ATTRIBUTES    **************************************************************

    @Column
    private int low;

    @Column
    private int high;

}
