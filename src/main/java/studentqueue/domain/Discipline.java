package studentqueue.domain;

import java.util.ArrayList;
import java.util.List;

import studentqueue.domain.AssignmentRange;
import studentqueue.utils.NonEmptyString;

import javax.persistence.*;

@Entity
public class Discipline
{
//******    CONSTRUCTOR    *****************************************************************


    public Discipline( NonEmptyString name )
    {
        this.name = name.getValue();
        this.description = "";

        this.studyForms = new ArrayList< StudyForm >();
        this.assignments = new ArrayList< Assignment >();
    }


    protected Discipline(){ }


//******    GETTERS    ******************************************************************


    public String getName()
    {
        return name;
    }


    public String getDescription()
    {
        return description;
    }


    public int getAssignmentsCount()
    {
        return assignments.size();
    }


    public long getLabAssignmentsCount()
    {
        return filterAssignmentsForCount( AssignmentType.LAB );
    }


    public long getTestAssignmentsCount()
    {
        return filterAssignmentsForCount( AssignmentType.TEST );
    }


    public StudyForm getStudyForms( int index )
    {
        return studyForms.get( index );
    }


    public int getStudyFormsCount()
    {
        return studyForms.size();
    }


    public long getLectureStudyFormsCount()
    {
        return filterStudyFormsForCount( StudyFormType.LECTURE_TYPE );
    }


    public long getLabStudyFormsCount()
    {
        return filterStudyFormsForCount( StudyFormType.LAB_TYPE );
    }


    public Assignment getAssignment( int index )
    {
        return assignments.get( index );
    }


//******    SETTERS     ****************************************************************


    public void setDescription( NonEmptyString description )
    {
        this.description = description.getValue();
    }


//******    PUBLIC METHODS    **********************************************************


    public void addAssignment( Assignment assignment )
    {
        this.assignments.add( assignment );
    }


    public void addStudyForm( StudyForm studyForm )
    {
        this.studyForms.add( studyForm );
    }


//******    PUBLIC METHODS TO FIND    ****************************************************


    public StudyForm findStudyForm( String name , StudyFormType studyFormType )
    {
        return studyForms.stream()
                .filter( lab -> lab.getName().equals( name ) && lab.getStudyFormType() == studyFormType )
                .findFirst().orElse( null );
    }


    public Assignment findAssignment( String name )
    {
        return assignments.stream()
            .filter( assignment -> assignment.getName().equals( name ))
            .findFirst().orElse( null );
    }


//**********************************************************************************************

/**
 * @return true if score of all assignments is in range ( 60 , 100 ). 
 * 60 - min score to pass the course
 * 100 - max score to pass the course 
 */
    public boolean isCorrectScore()
    {
        AssignmentRange assignRange =
                sumAssignmentsForCommonRangeofDiscipline( assignments );

        //AssignmentRange resultRange = labAssignRange.sumRanges( testAssignRange );
        return ( ( assignRange.getLow() == 60 ) && ( assignRange.getHigh() == 100 ) );
    }


    public void removeStudyForm( StudyForm studyForm )
    {
        if ( studyForm == null )
            throw new RuntimeException( "Study form doesn't exist" );
        if ( ! isStudyFormInDiscipline( studyForm ) )
            throw new RuntimeException( "This discipline hasn't this study form " );

        studyForms.remove( studyForm );
    }


    public void removeAssignment( Assignment assignment )
    {
        if( assignment == null )
            throw new RuntimeException( "This assignment " + assignment.getName() + " doesn't exist" );
        if ( ! assignments.contains( assignment ) )
            throw new RuntimeException( "This discipline hasn't assignment " + assignment.getName() );

        assignments.remove( assignment );
    }


    public boolean isStudyFormInDiscipline( StudyForm studyForm )
    {
        return studyForms.contains( studyForm );
    }


//******    PRIVATE METHODS     *******************************************************


    private < T extends Assignment > AssignmentRange
        sumAssignmentsForCommonRangeofDiscipline( List< T > assignments )
    {
        int low = 0;
        int high = 0;

        for ( T assignment : assignments )
        {
            low += assignment.getRangeOfAllLevels().getLow();
            high += assignment.getRangeOfAllLevels().getHigh();
        }

        return new AssignmentRange( low , high );
    }


    private long filterStudyFormsForCount( StudyFormType studyFormType )
    {
        return studyForms.stream()
                .filter( studyForm -> studyForm.getStudyFormType()
                        .equals( studyFormType ) )
                        .count();
    }


    private long filterAssignmentsForCount( AssignmentType assignmentType )
    {
        return assignments.stream()
                    .filter( assignment -> assignment.getType().equals( assignmentType ) )
                    .count();
    }

//******    ATTRIBUTES    *************************************************************


    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String description;

    @OneToMany( cascade = CascadeType.ALL )
    private List< StudyForm > studyForms;

    @OneToMany( cascade = CascadeType.ALL )
    private List< Assignment > assignments;
}
