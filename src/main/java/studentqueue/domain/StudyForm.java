package studentqueue.domain;

import studentqueue.utils.NonEmptyString;

import javax.persistence.*;

@Entity
@Inheritance(  strategy = InheritanceType.TABLE_PER_CLASS )
public class StudyForm
{
//******    CONSTRUCTOR    ******************************************************************


    public StudyForm( NonEmptyString name, StudyFormType studyFormType )
    {
        this.name = name.getValue();
        this.studyFormType = studyFormType;
        this.description = "";
    }


    protected StudyForm(){ }


//******    GETTERS    ******************************************************************
    
    
    public String getName() 
    {
        return name;
    }


    public String getDescription() 
    {
        return description;
    }


    public StudyFormType getStudyFormType ()
    {
        return studyFormType;
    }


    //******    SETTERS    ******************************************************************
    
    
    public void setDescription( NonEmptyString description )
    {
        this.description = description.getValue();
    }
    
    
//******    ATTRIBUTES    ********************************************************************


    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String description;
    private StudyFormType studyFormType;

}
