package studentqueue.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;

/**
 * Created by Valeria Zaychenko on 21.07.2016.
 */
@Configuration
public class TestRootContextConfiguration extends BaseRootContextConfiguration
{

    @Override
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean()
    {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();

        LocalContainerEntityManagerFactoryBean factory =
                new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter( adapter );
        factory.setPersistenceUnitName( "StudentQueueJUnit" );
        return factory;
    }
}
