package studentqueue.webcontroller;

/**
 * Created by lera on 06.04.2016.
 */
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import studentqueue.services.IGroupService;

import javax.inject.Inject;
import java.util.Map;

@Controller
public class HomeController
{
    @RequestMapping( path = { "/groups" } , method = RequestMethod.GET  )
    String home ( Map< String, Object > model )
    {
        model.put( "groups", this.groupService.viewGroups() );
        return "group";
    }

    @Inject
    private IGroupService groupService;
}
