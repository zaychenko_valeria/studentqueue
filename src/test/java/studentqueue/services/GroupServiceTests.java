package studentqueue.services;

import jdk.nashorn.internal.runtime.ECMAException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import studentqueue.config.TestRootContextConfiguration;
import studentqueue.services.IGroupService;
import studentqueue.services.impl.StudentService;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by Valeria Zaychenko on 20.07.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { TestRootContextConfiguration.class }  )
@Transactional
public class GroupServiceTests
{
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IStudentService studentService;

    @Test
    public void createGroupCorrectly()
    {
        String name = "KI-10-11";
        groupService.createGroup( name );

        assertNotNull( groupService.findGroup( name ) );
    }

    @Test( expected = Exception.class )
    public void doesNotCreateGroupWithNullName()
    {
        groupService.createGroup( null );
    }

    @Test( expected = Exception.class )
    public void doesNotCreateGroupWithEmptyName()
    {
        groupService.createGroup( "" );
    }

    @Test( expected = Exception.class )
    public void doesNotCreateGroupAlreadyExists()
    {
        String name = "KI-11-1";
        groupService.createGroup( name );
        groupService.createGroup( name );
    }

    @Test
    public void changeNameOfGroupProperly()
    {
        String name = "KI-10";
        String newName = "KI-15";
        groupService.createGroup( name );
        groupService.changeGroupName( name , newName );

        assertEquals( groupService.findGroup( newName ).getName() , newName );
    }

    @Test( expected = Exception.class )
    public void doesNotChangeNameOfGroupToNull()
    {
        String name = "KI-10";

        groupService.createGroup( name );
        groupService.changeGroupName( name , null );
    }

    @Test( expected = Exception.class )
    public void doesNotChangeNameOfGroupToEmpty()
    {
        String name = "KI-10";

        groupService.createGroup( name );
        groupService.changeGroupName( name , "" );
    }

    @Test
    public void addStudentCorrectly()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        assertNotNull( groupService.getAllStudentsUUIDs( groupName ) );
        assertTrue( groupService.isInGroup( uuid , groupName ) );
    }

    @Test( expected = Exception.class )
    public void doesNotAddStudentHasAlreadyBeenInGroup()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        groupService.addStudentToGroup( uuid , groupName );
    }

    @Test( expected = Exception.class )
    public void doesNotAddStudentToGroupDoesNotExist()
    {
        String groupName = "KI-00";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.addStudentToGroup( uuid , groupName );
    }

    @Test
    public void addStudentInTwoGroupsCorrectly()
    {
        String groupName = "KI-11";
        String groupName2 = "KI-15";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.createGroup( groupName2 );
        groupService.addStudentToGroup( uuid , groupName );
        groupService.addStudentToGroup( uuid , groupName2 );

        assertNotNull( groupService.getAllStudentsUUIDs( groupName ) );
        assertTrue( groupService.isInGroup( uuid , groupName ) );
        assertNotNull( groupService.getAllStudentsUUIDs( groupName2 ) );
        assertTrue( groupService.isInGroup( uuid , groupName2 ) );
    }

    @Test
    public void addTwoStudentsInGroupCorrectly()
    {
        String groupName = "KI-15";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        String uuid2 = studentService.createStudent( "Petr" , "Petrov" , "pp@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );
        groupService.addStudentToGroup( uuid2 , groupName );

        assertNotNull( groupService.getAllStudentsUUIDs( groupName ) );
        assertTrue( groupService.isInGroup( uuid , groupName ) );
        assertTrue( groupService.isInGroup( uuid2 , groupName ) );
    }

    @Test( expected = Exception.class )
    public void doesNotAddStudentWithWrongUUID()
    {
        String groupName = "KI-11";
        groupService.createGroup( groupName );
        UUID uuid = UUID.randomUUID();
        groupService.addStudentToGroup( uuid.toString() , groupName );
    }

    @Test( expected = Exception.class )
    public void doesNotAddStudentWithNullUUID()
    {
        String groupName = "KI-11";
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( null , groupName );
    }

    @Test
    public void createdStudentWasNotAddedToGroupIsNotInGroup()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );

        assertEquals( groupService.getAllStudentsUUIDs( groupName ).size() , 0 );
        assertFalse( groupService.isInGroup( uuid , groupName ) );
    }

    @Test
    public void removeStudentFromGroupCorrectly()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        groupService.removeStudentFromGroup( uuid , groupName );

        assertEquals( groupService.getAllStudentsUUIDs( groupName ).size() , 0 );
        assertFalse( groupService.isInGroup( uuid , groupName ) );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveFromGroupDoesNotExist()
    {
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.removeStudentFromGroup( uuid , "KI_222" );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveFromEmptyGroup()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );

        groupService.removeStudentFromGroup( uuid , groupName );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveStudentNullUUID()
    {
        String groupName = "KI-111";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        groupService.removeStudentFromGroup( uuid , null );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveStudentEmptyUUID()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        groupService.removeStudentFromGroup( uuid , "" );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveStudentTwice()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );
        groupService.addStudentToGroup( uuid , groupName );

        groupService.removeStudentFromGroup( uuid , groupName );
        groupService.removeStudentFromGroup( uuid , groupName );
    }

    @Test( expected = Exception.class )
    public void doesNotRemoveStudentNotInGroup()
    {
        String groupName = "KI-11";
        String uuid = studentService.createStudent( "Ivan" , "Ivanov" , "ii@ru" );
        groupService.createGroup( groupName );

        groupService.removeStudentFromGroup( uuid , groupName );
    }

    @Test
    public void deleteGroupCorrectly()
    {
        String groupName = "KI-11";
        groupService.createGroup( groupName );

        assertTrue( groupService.existGroup( groupName ) );

        groupService.deleteGroup( groupName );

        assertFalse( groupService.existGroup( groupName ) );
    }

    @Test( expected = Exception.class )
    public void doesNotDeleteGruopTwice()
    {
        String groupName = "KI-11";
        groupService.createGroup( groupName );

        groupService.deleteGroup( groupName );
        groupService.deleteGroup( groupName );
    }

    @Test( expected = Exception.class )
    public void doesNotDeleteGroupDoesNotExist()
    {
        groupService.deleteGroup( "777" );
    }
}
