# README #

This is a online system that helps to provide blended university courses. 

Technologies, frameworks, tools & versions:
* Java 1.8
* JUnit 4.12
* Hibernate 5.0.5 / JPA 2
* DB - MySQL 5.7 (InnoDB) / Embedded DB for testing - H2 1.4.192
* Spring Framework(MVC) 4.3.1
* Bootstrap Framework 3.3.5
* JavaScript + JQuery
* Maven

Patterns:
* Factory
* Singleton
* CRTP
* Repository
* MVC+MVVM

zaychenko.valeria@gmail.com