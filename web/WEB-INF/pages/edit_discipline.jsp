<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        table { margin-left:20px;
                margin-right:20px;
                border:thin solid black;
                caption-side:bottom;
              }
        th, td { border:thin solid black;
                 padding:5px;
               }
    </style>
</head>

<body>
    <!------------------------------------------------------------------------------------------------>
    <div id="offering_choose">
        <select>
        <!-- Options must be in quantity of subjects -->
            <option value="ipz">IPZ</option>
        </select>
        <select>
        <!-- Options must be in years of offering -->
            <option value="year">2015</option>
        </select>
    </div>
    <!------------------------------------------------------------------------------------------------>
    <div id="subjects_choose">
        <label for="lectures">Lectures:</label>
        <input type="number" name="lectures" required><br>
        <label for="labs">Labs:</label>
        <input type="number" name="labs" required>
    </div>
    <!------------------------------------------------------------------------------------------------>
    <div id ="discipline_levels">
        <table>
            <tr>
                <!-- TH must be in quantity of levels -->
                <th>Index</th>
                <th>Light</th>
                <th>Medium</th>
                <th>Advanced</th>
            </tr>
            <tr>
                <td>1</td>
                <td>
                    <input type="text" name="light" required placeholder="0-6"><br>
                </td>
                <td>
                    <input type="text" name="medium" required placeholder="6-12"><br>
                </td>
                <td>
                    <input type="text" name="advanced" required placeholder="12-15"><br>
                </td>
            </tr>
        </table>
    </div>
    <!------------------------------------------------------------------------------------------------>
    <div id="discipline_groups">
        <label for="groups">Groups:</label>
        <ul>
            <!-- List must be in quantity of groups -->
            <li>KI-10-5</li>
            <li>KI-13-1</li>
        </ul>
    </div>
    <!------------------------------------------------------------------------------------------------>
    <button type="button">Add new discipline</button>
    <button type="button">Add assistant</button>
</body>
</html>