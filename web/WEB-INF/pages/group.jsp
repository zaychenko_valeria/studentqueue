<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="groups" type="java.util.List<studentqueue.viewmodel.AcademicGroupBriefView>"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<template:basic htmlTitle="Groups" bodyTitle="Groups">
    <c:choose>
        <c:when test="${fn:length(groups) == 0}">
            <i>There are no groups in the system.</i>
        </c:when>
        <c:otherwise>
            <c:forEach items="${groups}" var="group">
                AcademicGroup ${group.name}
                <br>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</template:basic>