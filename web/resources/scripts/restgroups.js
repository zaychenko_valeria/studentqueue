/**
 * Created by lera on 18.07.2016.
 */

$(document).ready(function() {
    $.ajax({
        url: "/rest/groups",
        type: 'GET',
        headers: {
            Accept : "application/json; charset=utf-8"
        }
    }).then(function(data) {
        for ( var i = 0; i < data.length; i++ )
        {
            //var element = createTemplate(data[i]);
            var element = $('<div class="font">' + data[i] + '</div>' +
                            '<button type="button" class="btn btn-default btn-lg" id="loadModalEditsGroup">Edit group</button>');
            $('body').append(element);
        }

    });
    $("#loadModalCreatingGroup").click(function(){
        $("#addGroup").modal();
    });
    $("#createGroup").click(function(){
        var groupName = document.getElementById("groupName").value;
        if ( validateGroupName( groupName ) ) {
            createGroup( groupName );
        }
    });
});

function validateGroupName(data) {
    if (data == null || data == "") {
        alert("Name must be filled out");
        return false;
    }
    return true;
}

function createGroup(groupName) {
    var data = {
        "name" : groupName
    }
    $.ajax({
        type: "POST",
        url: "/rest/groups",
        data: JSON.stringify(data),     /*"name=" + $.toJSON(groupName),*/
        contentType: "application/json; charset=utf-8",
        success: function(data){alert("Group was created successfully");},
        failure: function(errMsg) {
            alert(errMsg);
        }
    }).then(function() {
        var element = $('<div class="font">' + groupName + '</div>');
        $('body').append(element);
    });
}

/*function createTemplate(groupName) {
    var template = $('#template').html();
    Mustache.parse(template);   // optional, speeds up future uses
    var rendered = Mustache.render(template, {groupName: groupName});
    $('#target').html(rendered);
}
*/
